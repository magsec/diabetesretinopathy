__author__ = 'marcos'
import os
import numpy as np
import random
from pandas import DataFrame
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from joblib import Parallel, delayed
from src.abstracts.Abstract import Abstract
from src.tools.Paths import TRAIN_DATA
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.abstracts.Industry import Industry
from src.abstracts.Observation import Patch
from src.machines.NormalDistribution import NormalDistribution


def _lr_par_predict(logistic_regressions, obs):
    x = []
    for lr in logistic_regressions:
        x += lr.predict_proba(obs).flatten().tolist()
    return x


class ExpertIndustry(Industry):

    def __init__(self, eye_iterator, address, n_components=64, name="ExpertIndustry"):
        self.eye_iterator = eye_iterator
        self.n_components = n_components
        self.distributions = {} # normal distribution for
        self.pca = PCA(n_components=n_components)
        self.logistic_regressions = []
        class_weight = {0: 36.45, 1: 3.45, 2: 7.47, 3: 1.23, 4: 1.0}
        self.rf = RandomForestClassifier(n_estimators=300, verbose=1, n_jobs=-1,
                                         oob_score=True, class_weight=class_weight)

        self.dist_trained = False
        self.pca_trained = False
        self.lr_trained = False
        self.rf_trained = False
        Industry.__init__(self, name=name, address=address)

    def get_predictors(self, data):
        '''
        Unsupervised lerning: applies PCA, computes mean/covariance, and distribution features
        :param data: sparse codings
        :return: a column matrix with new features
        '''
        # apply pca
        data = self.pca.transform(data)
        mean = np.matrix(np.mean(data, axis=0)).T
        cov = np.matrix(np.cov(data.T))
        assert(mean.shape == (self.n_components, 1))

        predictors = [d.features(mean, cov) for d in self.distributions]

        # no sum aggregation
        mean = (mean.T*mean)[0, 0]
        cov = np.sum(np.diag(cov))

        predictors.append([mean, cov])
        predictors = np.array(predictors)

        predictors = np.matrix(predictors.flatten()).T
        return predictors

    def train(self, n_eyes_pca=10000, n_patches_pca=20,  n_eyes_distributions=2000, n_eyes_lr=20000, n_eyes_rf=20000):
        # compute PCA
        if not self.pca_trained:
            self.train_pca(n_eyes=n_eyes_pca, n_patches=n_patches_pca)
            self.save()

        # compute normal distributions
        if not self.dist_trained:
            set_of_stages = [[0], [1], [2], [3], [4], [0, 1], [1, 2], [2, 3], [3, 4], [0, 1, 2], [1, 2, 3], [2, 3, 4]]
            self.distributions = [self.train_distributions(stages=stages, n_eyes=n_eyes_distributions)
                                  for stages in set_of_stages]
            self.dist_trained = True
            self.save()

        # train logistic regression
        if not self.lr_trained:
            self.logistic_regressions = self.train_logistic_regression(n_eyes=n_eyes_lr)
            self.save()
        # train forest classifiers
        if not self.rf_trained:
            self.train_random_forest(n_eyes=n_eyes_rf)
            self.save()

    def train_pca(self, n_eyes=20000, n_patches=10):
        print "####################### Start Training PCA #######################"
        count = 0
        # Generate data
        patch = self.eye_iterator.next_test_pickle()
        sdf = patch.predictors
        rows = random.sample(sdf.index, n_patches)
        x = sdf.ix[rows]

        patch = self.eye_iterator.next_test_pickle()
        count += 1

        while count < n_eyes and patch is not None:
            sdf = patch.predictors
            rows = random.sample(sdf.index, n_patches)
            x = np.vstack((x, sdf.ix[rows]))
            patch = self.eye_iterator.next_test_pickle()
            count += 1

        self.pca.fit(sdf)
        self.pca_trained = True
        print "Explained Variance: ", sum(self.pca.explained_variance_ratio_)
        self.eye_iterator.reset()

    def train_distributions(self, stages=[0], n_eyes=2000):
        print "####################### Start Training Distribution #######################"
        distribution = NormalDistribution(dimension=self.n_components)
        count = 0

        modulo = len(stages)
        i = 0
        stage = stages[i]
        patch = self.eye_iterator.next_train_pickle(stage=stage)

        while count < n_eyes and patch is not None:
            sdf = patch.predictors
            assert(stage == patch.response)

            # transform and train
            data = self.pca.transform(sdf)
            distribution.train(data)

            # get next data
            count += 1
            i += 1
            i %= modulo
            stage = stages[i]
            patch = self.eye_iterator.next_train_pickle(stage=stage)

        # update stats and reset iterator
        distribution.update_stats()
        self.eye_iterator.reset()
        return distribution

    def lr_train_data(self, n_eyes=20000):
        x = []
        y = []
        count = 0
        stage = 0
        patch = self.eye_iterator.next_train_pickle(stage=stage)

        while count < n_eyes and patch is not None:
            sdf = patch.predictors
            assert(stage == patch.response)

            predictors = self.get_predictors(data=sdf)
            assert(predictors.shape[1] == 1)
            assert(isinstance(predictors, np.matrix))
            x.append(predictors.flatten().tolist()[0])
            y.append(stage)

            # get next data
            count += 1
            stage += 1
            stage %= 5
            patch = self.eye_iterator.next_train_pickle(stage=stage)


        # update stats and reset iterator
        self.eye_iterator.reset()
        return np.array(x), np.array(y)

    def train_logistic_regression(self, n_eyes=20000):
        print "####################### Start Training Logistic Regression #######################"
        x, y = self.lr_train_data(n_eyes=n_eyes)
        print "x.shape", x.shape
        print "y.shape", y.shape

        class_weight = {0: 36.45, 1: 3.45, 2: 7.47, 3: 1.23, 4: 1.0}
        lr1 = LogisticRegression(penalty="l1", class_weight=class_weight)
        lr1 = lr1.fit(x, y)
        print "LR one-vs-all score:", lr1.score(x, y)
        print "Coefficients: ", lr1.coef_

        y2 = np.logical_or(y == 0, y == 1)
        class_weight = {True: 4.11, False: 1}
        lr2 = LogisticRegression(penalty="l1", class_weight=class_weight)
        lr2 = lr2.fit(x, y2)
        print "LR 01-vs-234 score:", lr2.score(x, y2)
        print "Coefficients: ", lr2.coef_

        y3 = np.logical_or(y == 1, y == 2)
        class_weight = {True: 1, False: 3.54}
        lr3 = LogisticRegression(penalty="l1", class_weight=class_weight)
        lr3 = lr3.fit(x, y3)
        print "LR 12-vs-034 score:", lr3.score(x, y3)
        print "Coefficients: ", lr3.coef_

        y4 = np.logical_or(y == 2, y == 3)
        class_weight = {True: 1, False: 4.69}
        lr4 = LogisticRegression(penalty="l1", class_weight=class_weight)
        lr4 = lr4.fit(x, y4)
        print "LR 23-vs-014 score:", lr4.score(x, y4)
        print "Coefficients: ", lr4.coef_

        y5 = np.logical_or(y == 3, y == 4)
        class_weight = {True: 1, False: 21.21}
        lr5 = LogisticRegression(penalty="l1", class_weight=class_weight)
        lr5 = lr5.fit(x, y5)
        print "LR 34-vs-012 score:", lr5.score(x, y5)
        print "Coefficients: ", lr5.coef_

        return [lr1, lr2, lr3, lr4, lr5]

    def lr_predict(self, obs):
        x = []
        for lr in self.logistic_regressions:
            x += lr.predict_proba(obs).flatten().tolist()
        return x

    def rf_train_data(self, n_eyes=20000):
        print "####################### Preparing Random Forest Training Data #######################"
        x, y = self.lr_train_data(n_eyes=n_eyes)
        n_obs = x.shape[0]
        print "x.shape", x.shape
        print "y.shape", y.shape
        x = x.tolist()
        x = Parallel(n_jobs=-1)(
            delayed(_lr_par_predict)(
                self.logistic_regressions,
                obs)
            for obs in x)
        x = np.array(x)
        assert(x.shape[0] == n_obs)
        return x, y

    def train_random_forest(self, n_eyes):
        print "####################### Start Training Random Forests #######################"
        x, y = self.rf_train_data(n_eyes=n_eyes)
        print "x.shape", x.shape
        print "y.shape", y.shape
        self.rf = self.rf.fit(x, y)
        self.rf_trained = True
        print "Forest Score", self.rf.oob_score_

    def predict(self, data):
        '''
        Computes class probability for an eye
        :param data: data frame containing sparse code of an eye
        :return: a Patch
        '''
        predictors = self.get_predictors(data=data)

        # apply logistic regressions
        predictors = self.lr_predict(predictors.T)

        # apply random forest
        predictors = self.rf.predict_proba(predictors)
        response = np.argmax(predictors)

        return Patch(response=response, predictors=predictors)

    def produce_test(self):
        '''
        Predicts eye label for each eye in test data set
        :return: Nothing. Predictions stored in product dictionary
        '''
        count = 0
        patch = self.eye_iterator.next_test_pickle()
        while patch is not None:
            product = self.predict(patch.predictors)
            self.product[patch.patch_id] = product
            print "\tPredicted label: %d" % product.response
            print "\tPredicted probabilities:", product.predictors

            patch = self.eye_iterator.next_test_pickle()
            if count % 1000 == 0:
                self.save()
            count += 1

    def export_production(self):
        '''
        Writes the predicted labels to a csv file
        :return: Nothing. File saved at self.get_full_address
        '''
        result = [[k, self.product[k].response] for k in self.product]
        df = DataFrame(data=result, columns=['image', 'level'])
        out_path = os.path.join(self.get_full_address(), 'submission.csv')
        if not os.path.exists(path=self.get_full_address()):
            os.makedirs(self.get_full_address())
        df.to_csv(out_path, index=False)


if __name__ == "__main__":
    load = False

    if load:
        file_name = "/home/marcos/Universum/Studium/MasterThesis/results/Diabetes/Library/" \
               "Dictionary 64x512 - 0.000800 penalty/ExpertIndustry.pkl"
        industry = Abstract.load(file_name=file_name)
    else:
        data_file = TRAIN_DATA
        path = "/home/marcos/Universum/Studium/MasterThesis/results/Diabetes/Library/" \
               "Dictionary 64x512 - 0.000800 penalty"
        train_path = os.path.join(path, "train")
        test_path = os.path.join(path, "test")
        eye_it = EyeImageIterator(data_file, train_path=train_path, test_path=test_path, file_endings=".pkl")
        industry = ExpertIndustry(eye_it, address=path, n_components=512)
    industry.train(n_eyes_pca=5000, n_patches_pca=100, n_eyes_distributions=1000, n_eyes_lr=3500, n_eyes_rf=3500)
    industry.save()
    industry.produce_test()
    industry.save()
    industry.export_production()
