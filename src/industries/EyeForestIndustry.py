__author__ = 'marcos'
import os

import numpy as np
from pandas import DataFrame
from math import log
from sklearn.decomposition import PCA
from sklearn.ensemble.forest import RandomForestClassifier
from src.abstracts.Industry import Industry
from src.abstracts.Observation import Patch
from src.dataIterators.SparsePatchIterator import SparsePatchIterator
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.tools.Paths import TRAIN_DATA, GREEN_TRAIN, GREEN_TEST, DIABETES_LIBRARY
from src.explorers.DictionaryExplorer import DictionaryExplorer


class EyeForestIndustry(Industry):

    def __init__(self, eye_iterator, dictionary, n_obs=5000, name="EyeForestIndustry", address=None):
        '''

        :param eye_iterator: an iterator over the data
        :param dictionary: a Dictionary object
        :param n_obs: number of observations used for training the random forest
        :param name:
        :param address:
        :return:
        '''
        print "Starting EyeForestIndustry"
        self.n_obs = n_obs
        self.eye_iterator = eye_iterator
        self.dictionary = dictionary

        self.sparse_iterator = SparsePatchIterator(dictionary, eye_iterator.data_file, eye_iterator.train_path,
                                                   eye_iterator.test_path, file_endings=eye_iterator.file_endings)
        # self.pool = Pool()
        self.pca, self.forest = self.pre_train(n_components=40)

        # dictionary: key=eye_id, value=Patch object
        self.product = {}

        # number of observations for training random forest
        if address is None:
            address = self.dictionary.get_full_address()
        Industry.__init__(self, name, address)

    def produce_test(self):
        '''
        Computes the posterior probability of the labels for each eye
        :return:
        '''
        count = 0
        eye = self.eye_iterator.next_test_observation()
        while eye is not None:
            list_of_obs = eye.patches(size=self.dictionary.predictors_size)
            list_of_obs = [_.predictors for _ in list_of_obs]

            product = self.predict(list_of_obs)
            self.product[eye.name] = product
            print "\tPredicted label: %d" % product.response
            print "\tPredicted log likelihoods:\n", product.predictors

            eye = self.eye_iterator.next_test_observation()
            if count % 1000 == 0:
                self.save()
            count += 1

    def export_production(self):
        result = [[k, self.product[k].response] for k in self.product]
        df = DataFrame(data=result, columns=['image', 'level'])
        out_path = os.path.join(self.get_full_address(), 'submission.csv')
        if not os.path.exists(path=self.get_full_address()):
            os.makedirs(self.get_full_address())
        df.to_csv(out_path, index=False)

    def predict(self, list_of_obs):
        '''
        Computes the patch with log-
        :param list_of_obs: a list of predictors
        :return: a prediction patch
        '''
        list_of_obs = self.dictionary.par_predict(list_of_obs=list_of_obs, n_jobs=-1)
        data = np.matrix(list_of_obs)
        print "\tData shape:", data.shape
        data = self.pca.transform(data)
        print "\tPCA shape:", data.shape

        # [n_samples, n_classes]
        data = self.forest.predict_log_proba(data)
        print "\tForest shape:", data.shape

        # substitute negative infinities by small numbers
        data[np.isneginf(data)] = -log(self.n_obs)
        predictors = np.sum(data, axis=0).T
        response = np.argmax(predictors)
        return Patch(response=response, predictors=predictors)

    def pre_train(self, n_components):
        # generate data
        print "Generating data"
        list_of_patches = self.sparse_iterator.train_list(n_obs=self.n_obs, n_patches=500)
        print "Data set has length ", len(list_of_patches)
        x = np.array([_.predictors.flatten().tolist()[0] for _ in list_of_patches])

        y = np.array([_.response for _ in list_of_patches])
        for i in range(5):
            print "Number of patches labeled as %d is %f" % (i, np.sum(y == i))
        # use data to train pca
        pca = PCA(n_components=n_components)
        print "Training PCA"
        pca = pca.fit(x)
        print "Explained Variance: ", sum(pca.explained_variance_ratio_)

        x = pca.transform(x)
        print "Training Forest"
        forest = RandomForestClassifier(n_estimators=30, verbose=1, n_jobs=-1, oob_score=True)
        forest = forest.fit(x, y)
        print "Forest Score", forest.oob_score_
        del x, y
        return pca, forest

    def show(self):
        result = [[k, self.product[k].response] for k in self.product]
        df = DataFrame(data=result, columns=['image', 'level'])
        for i in range(5):
            np.sum(df.iloc[:, 1] == i)




if __name__ == "__main__":
    eye_it = EyeImageIterator(TRAIN_DATA, GREEN_TRAIN, GREEN_TEST, file_endings=".png")
    dic = DictionaryExplorer(DIABETES_LIBRARY).choose_dictionary()
    industry = EyeForestIndustry(eye_iterator=eye_it, dictionary=dic, n_obs=200000)
    industry.produce_test()
    industry.save()
    industry.export_production()

