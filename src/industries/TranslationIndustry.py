__author__ = 'marcos'
import os
from time import time
from pandas import SparseDataFrame
from copy import deepcopy

from src.abstracts.Industry import Industry
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.explorers.DictionaryExplorer import DictionaryExplorer
from src.tools.Paths import DIABETES_LIBRARY, TRAIN_DATA, GREEN_TEST, GREEN_TRAIN


class TranslationIndustry(Industry):

    def __init__(self, eye_iterator, dictionary, name="TranslationIndustry", address=None, n_patches=1000):
        '''

        :param eye_iterator: an iterator over the data
        :param machines: a list of machines
        :param name:
        :param address:
        :return:
        '''
        self.eye_iterator = eye_iterator
        self.dictionary = dictionary
        self.n_patches = n_patches

        if address is None:
            address = self.dictionary.get_full_address()
        if not os.path.exists(path=address):
            os.makedirs(address)

        self.test_address = os.path.join(address, "test")
        if not os.path.exists(path=self.test_address):
            os.makedirs(self.test_address)
        self.train_address = os.path.join(address, "train")
        if not os.path.exists(path=self.train_address):
            os.makedirs(self.train_address)

        Industry.__init__(self, name, address)

    def produce_test(self):
        # for each eye, compute sparse code and save as csv
        count = 0
        eye = self.eye_iterator.next_test_observation(n_patches=self.n_patches)
        while eye is not None:
            list_of_obs = eye.patches(size=self.dictionary.predictors_size)
            list_of_obs = [_.predictors for _ in list_of_obs]

            start = time()
            product = self.predict(list_of_obs)
            end = time()-start
            print "\tPrediction duration: %.2f" % end

            # store the sparse code
            out_path = os.path.join(self.test_address, eye.name+".pkl")
            product.to_pickle(out_path)

            eye = self.eye_iterator.next_test_observation()
            if count % 1000 == 0:
                self.save()
            count += 1

    def produce_train(self):
        count = 0
        eye = self.eye_iterator.next_train_observation(n_patches=self.n_patches)
        while eye is not None:
            list_of_obs = eye.patches(size=self.dictionary.predictors_size)
            list_of_obs = [_.predictors for _ in list_of_obs]

            start = time()
            product = self.predict(list_of_obs)
            end = time()-start
            print "\tPrediction duration: %.2f" % end

            # store the sparse code
            out_path = os.path.join(self.train_address, eye.name+".pkl")
            product.to_pickle(out_path)

            eye = self.eye_iterator.next_train_observation()
            if count % 1000 == 0:
                self.save()
            count += 1

    def predict(self, list_of_obs):
        '''
        Computes sparse coding of observation
        :param list_of_obs: list with the observations
        :return: a data frame with the sparse codes
        '''
        list_of_obs = self.dictionary.par_predict(list_of_obs=list_of_obs, n_jobs=-1)
        data = SparseDataFrame(list_of_obs, default_fill_value=0)
        print "\tData shape:", data.shape
        return data


if __name__ == "__main__":
    eye_it = EyeImageIterator(TRAIN_DATA, GREEN_TRAIN, GREEN_TEST, file_endings=".png")
    dic = DictionaryExplorer(DIABETES_LIBRARY).choose_dictionary()
    industry = TranslationIndustry(eye_iterator=eye_it, dictionary=dic, n_patches=1000)
    industry.produce_train()
    industry.produce_test()
