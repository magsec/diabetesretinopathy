__author__ = 'marcos'
__author__ = 'marcos'
import os
import numpy as np
import random
from pandas import DataFrame
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from joblib import Parallel, delayed
from src.abstracts.Abstract import Abstract
from src.tools.Paths import TRAIN_DATA
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.abstracts.Industry import Industry
from src.abstracts.Observation import Patch
from src.machines.NormalDistribution import NormalDistribution


def _lr_par_predict(logistic_regressions, obs):
    x = []
    for lr in logistic_regressions:
        x += lr.predict_proba(obs).flatten().tolist()
    return x


class ForestIndustry(Industry):

    def __init__(self, eye_iterator, address, n_components=64, name="ForestIndustry"):
        self.eye_iterator = eye_iterator
        self.n_components = n_components
        self.pca = PCA(n_components=n_components)
        self.rf = RandomForestClassifier(n_estimators=300, verbose=1, n_jobs=-1, oob_score=True)

        self.pca_trained = False
        self.rf_trained = False
        Industry.__init__(self, name=name, address=address)

    def train(self, n_eyes_pca=10000, n_patches_pca=20, n_eyes_rf=20000):
        # compute PCA
        if not self.pca_trained:
            self.train_pca(n_eyes=n_eyes_pca, n_patches=n_patches_pca)
            self.save()

        # train forest classifiers
        if not self.rf_trained:
            self.train_random_forest(n_eyes=n_eyes_rf)
            self.save()

    def train_pca(self, n_eyes=20000, n_patches=10):
        print "####################### Start Training PCA #######################"
        count = 0

        # Generate data
        patch = self.eye_iterator.next_test_pickle()
        sdf = patch.predictors
        rows = random.sample(sdf.index, n_patches)
        x = sdf.ix[rows]

        patch = self.eye_iterator.next_test_pickle()
        count += 1
        while count < n_eyes and patch is not None:
            sdf = patch.predictors
            rows = random.sample(sdf.index, n_patches)
            x = np.vstack((x, sdf.ix[rows]))
            patch = self.eye_iterator.next_test_pickle()
            count += 1

        self.pca.fit(sdf)
        self.pca_trained = True
        print "Explained Variance: ", sum(self.pca.explained_variance_ratio_)
        self.eye_iterator.reset()

    def rf_train_data(self, n_eyes=20000):
        print "####################### Preparing Random Forest Training Data #######################"
        # TODO: CHANGE THIS
        x, y = self.lr_train_data(n_eyes=n_eyes)
        n_obs = x.shape[0]
        print "x.shape", x.shape
        print "y.shape", y.shape
        x = x.tolist()
        x = Parallel(n_jobs=-1)(
            delayed(_lr_par_predict)(
                self.logistic_regressions,
                obs)
            for obs in x)
        x = np.array(x)
        assert(x.shape[0] == n_obs)
        return x, y

    def train_random_forest(self, n_eyes):
        print "####################### Start Training Random Forests #######################"
        # TODO: CHANGE THIS
        x, y = self.rf_train_data(n_eyes=n_eyes)
        print "x.shape", x.shape
        print "y.shape", y.shape
        self.rf = self.rf.fit(x, y)
        self.rf_trained = True
        print "Forest Score", self.rf.oob_score_

    def predict(self, data):
        '''
        Computes class probability for an eye
        :param data: data frame containing sparse code of an eye
        :return: a Patch
        '''
        # TODO: apply pca to data

        # apply random forest
        data = self.rf.predict_proba(data)

        # TODO: aggregate results
        predictors = data
        response = np.argmax(predictors)

        return Patch(response=response, predictors=predictors)

    def produce_test(self):
        '''
        Predicts eye label for each eye in test data set
        :return: Nothing. Predictions stored in product dictionary
        '''
        count = 0
        patch = self.eye_iterator.next_test_pickle()
        while patch is not None:
            product = self.predict(patch.predictors)
            self.product[patch.patch_id] = product
            print "\tPredicted label: %d" % product.response
            print "\tPredicted probabilities:", product.predictors

            patch = self.eye_iterator.next_test_pickle()
            if count % 1000 == 0:
                self.save()
            count += 1

    def export_production(self):
        '''
        Writes the predicted labels to a csv file
        :return: Nothing. File saved at self.get_full_address
        '''
        result = [[k, self.product[k].response] for k in self.product]
        df = DataFrame(data=result, columns=['image', 'level'])
        out_path = os.path.join(self.get_full_address(), 'submission.csv')
        if not os.path.exists(path=self.get_full_address()):
            os.makedirs(self.get_full_address())
        df.to_csv(out_path, index=False)


if __name__ == "__main__":
    load = False

    if load:
        file_name = "/home/marcos/Universum/Studium/MasterThesis/results/Diabetes/Library/" \
               "Dictionary 64x512 - 0.000800 penalty/ExpertIndustry.pkl"
        industry = Abstract.load(file_name=file_name)
    else:
        data_file = TRAIN_DATA
        path = "/home/marcos/Universum/Studium/MasterThesis/results/Diabetes/Library/" \
               "Dictionary 64x512 - 0.000800 penalty"
        train_path = os.path.join(path, "train")
        test_path = os.path.join(path, "test")
        eye_it = EyeImageIterator(data_file, train_path=train_path, test_path=test_path, file_endings=".pkl")
        industry = ForestIndustry(eye_it, address=path, n_components=512)
    industry.train(n_eyes_pca=5000, n_patches_pca=100, n_eyes_rf=3500)
    industry.save()
    industry.produce_test()
    industry.save()
    industry.export_production()
