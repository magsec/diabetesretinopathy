__author__ = 'marcos'
from src.abstracts.Industry import Industry


class ForecastIndustry(Industry):

    def __init__(self, data_iterator, machines, name="ForecastIndustry", address="tests"):
        '''

        :param data_iterator: an iterator over the data
        :param machines: a list of machines
        :param name:
        :param address:
        :return:
        '''
        self.data_iterator = data_iterator
        self.machines = machines

        self.product_id = []
        self.product = []

        Industry.__init__(self, name, address)

    def produce(self):
        product = self.data_iterator.next_patch()
        self.product_id.append(product.identification())

        for machine in self.machines:
            product = machine.predict(product)

        self.product.append(product)

    def export_production(self):
        pass