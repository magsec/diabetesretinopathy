__author__ = 'marcos'
from src.abstracts.Industry import Industry
import multiprocessing


class DriverIndustry(Industry):

    def __init__(self, driver_iterator, trip_distribution, address=None, name="DriverIndustry"):
        if address is None:
            address = trip_distribution.address
        Industry.__init__(self, name=name, address=address)

        self.driver_iterator = driver_iterator
        self.trip_distribution = trip_distribution

        # TODO
        self.trip_patch_size = 3

        # dictionary: key=id, value=patch object
        self.product = {}

    def produce_test(self):
        # iterate drivers
        # for each driver, iterate trips, computing their sparse codes
        # compute the distribution of a driver
        # compute probabilities of belonging to driver
        # save object
        pass

    def export_production(self):
        pass