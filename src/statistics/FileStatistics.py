__author__ = 'marcos'
import os
import glob
import time

import numpy as np
import pandas as pd
from skimage.io import imread
from pandas import DataFrame

from src.settings import Configs
from src.tools.Logger import log_info, log_error


def main():
    compute_test_statistics()


def compute_train_statistics():
    def compute_statistics(image_label):
        try:
            # image_label = ['123_left','1']
            file_name = image_label[0] + ".jpeg"
            patient_id = str.split(image_label[0], "_")[0]
            side = str.split(image_label[0], "_")[1]
            label = image_label[1]

            file_path = os.path.join(Configs.train_data, file_name)
            im = imread(file_path)
            file_stats = FileStatistics(im, label, side, patient_id)
            dict_res = file_stats.get_statistics()

            log_info(patient_id)
            return pd.Series(dict_res)
        except Exception as e:
            log_error(e)
            return pd.Series()

    start_time = time.clock()

    labels = pd.read_csv(os.path.join(Configs.train_data, "labels.csv"))

    dat = labels.apply(compute_statistics, 1)

    dat.to_csv(Configs.train_statistics, index=False)

    print time.clock() - start_time


def compute_test_statistics():
    def compute_statistics(series):
        try:
            file_name = series[0]
            patient_id = file_name.split("/")[-1].split("_")[0]
            side = file_name.split("/")[-1].split("_")[1].split(".")[0]
            label = 5

            file_path = os.path.join(Configs.test_data, file_name)
            im = imread(file_path)
            file_stats = FileStatistics(im, label, side, patient_id)
            dict_res = file_stats.get_statistics()

            log_info(file_name)
            log_info(patient_id)
            log_info(side)
            return pd.Series(dict_res)
        except Exception as e:
            log_error(e)
            return pd.Series()

    start_time = time.clock()

    files = DataFrame(glob.glob(os.path.join(Configs.test_data, "*.*")))
    dat = files.apply(compute_statistics, axis=1)

    dat.to_csv(Configs.test_statistics, index=False)

    print time.clock() - start_time


class FileStatistics:
    '''
    Extracts simple statistics from an image.
    Given an image, it extracts statistics like
        - side: left or right eye
        - size: total number of pixels
        - shape_x: number of pixels in x axis
        - shape_y: number of pixels in y axis
        - label: stage of diabetes
        - eye_area: area of the eye
        - eye_radius: radius of the eye
        - id: id of the patient
        - percentiles of the color distribution
    '''
    LOW_INTENSITY = 5

    @staticmethod
    def eye_area(im):
        '''
        Estimates the number of the pixels inside the eye
        :param im:
        :return: area
        '''
        intensities = np.mean(im, axis=2).ravel()

        return (intensities > FileStatistics.LOW_INTENSITY).sum()

    @staticmethod
    def eye_radius(im):
        '''
        Estimates the radius of the eye
        :param im:
        :return:
        '''

        # gray image
        intensities = np.mean(im, axis=2)

        def nr_non_black_pixels(x):
            return (x > FileStatistics.LOW_INTENSITY).sum()

        non_black_pixels_vertically = np.apply_along_axis(nr_non_black_pixels, 0, intensities)
        non_black_pixels_horizontally = np.apply_along_axis(nr_non_black_pixels, 1, intensities)

        non_black_pixels_slice = np.hstack([non_black_pixels_horizontally, non_black_pixels_vertically])

        return max(non_black_pixels_slice)/2.0

    @staticmethod
    def get_eye_color_distribution(im_color, percentiles):
        '''

        :param im_color: 2 dimensional ndarray
        :return:
        '''
        intensities = im_color.ravel()
        intensities = intensities[intensities > FileStatistics.LOW_INTENSITY]
        return np.percentile(intensities, percentiles)

    def __init__(self, im, label, side, id):
        self.im = im
        self.side = side
        self.size = im.size
        self.shape = im.shape
        self.label = label
        self.id = id

    def get_statistics(self, percentiles=[0, 10, 25, 40, 50, 60, 75, 90, 100]):

        area = FileStatistics.eye_area(self.im)
        radius = FileStatistics.eye_radius(self.im)

        red_distribution = FileStatistics.get_eye_color_distribution(self.im[:, :, 0], percentiles)
        green_distribution = FileStatistics.get_eye_color_distribution(self.im[:, :, 1], percentiles)
        blue_distribution = FileStatistics.get_eye_color_distribution(self.im[:, :, 2], percentiles)

        res = {
            'side': self.side,
            'size': self.size,
            'shape_x': self.shape[0],
            'shape_y' : self.shape[1],
            'label': self.label,
            'eye_area': area,
            'eye_radius': radius,
            'id': self.id
            }
        red_labels = ["red_" + str(i) for i in percentiles]
        res.update(dict(zip(red_labels, red_distribution)))

        green_labels = ["green_" + str(i) for i in percentiles]
        res.update(dict(zip(green_labels, green_distribution)))

        blue_labels = ["blue_" + str(i) for i in percentiles]
        res.update(dict(zip(blue_labels, blue_distribution)))

        return res



if __name__ == '__main__':
    main()
