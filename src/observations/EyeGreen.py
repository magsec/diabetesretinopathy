__author__ = 'marcos'



from skimage.io import imread
from matplotlib import pyplot as plt
from pylab import cm

from src.tools.ImageFormatter import ImageFormatter
from src.observations.Eye import Eye
from src.tools import Logger


class EyeGreen(Eye):

    def __init__(self, file_path, label=None, address=None, name=None):

        Logger.log_status("Initializing a %s object" % EyeGreen.__name__)
        Eye.__init__(self, file_path, label=label, address=address)

    def process_image(self):
        '''
        Make all necessary image processing steps:
        crops eye, adjust brightness and contrasts,
        make the image gray
        :return:
        '''
        Logger.log_status("%s.%s" % (EyeGreen.__name__, EyeGreen.process_image.__name__))
        output_size = 500

        self.image = ImageFormatter.crop_eye(self.image)
        if not ImageFormatter.is_gray(self.image):
            self.image = self.image[:, :, 1]
        if max(self.image.shape) != output_size:
            self.image = ImageFormatter.resize_image(self.image, output_size=output_size)




