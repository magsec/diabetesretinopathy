__author__ = 'marcos'
import os
import random
from src.abstracts.Observation import Observation
from src.tools.TripFormatter import TripFormatter


class Driver(Observation):

    def __init__(self, driver_path, driver_id, address="test", name="Driver"):
        Observation.__init__(self, file_path=driver_path, name=name, address=address, label=driver_id)
        self.driver_path = driver_path
        self.driver_id = driver_id
        trip_paths = os.listdir(driver_path)

        self.trips = [TripFormatter.get_trip(trip_path) for trip_path in trip_paths]
        self.trip_iterator = 0

    def next(self):
        '''
        Iterate through the list of trips, returning the next trip or None if finished
        :return:
        '''
        if self.trip_iterator >= len(self.trips):
            return None
        else:
            trip = self.trips[self.trip_iterator]
            self.trip_iterator += 1
            return trip

    def next_random_patch(self, size=20):
        '''
        Returns a random trip patch from this driver
        :param size: size of the patch. Should be divisible by 2
        :return: a TripPatch object
        '''
        trip = random.choice(self.trips)
        return trip.next_random_patch(size=size)
