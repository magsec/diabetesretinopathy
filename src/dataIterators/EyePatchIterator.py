__author__ = 'marcos'
from time import time
from src.abstracts.DataIterator import DataIterator
from src.observations.EyePatch import EyePatch
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.tools import Logger
from random import shuffle


class EyePatchIterator(DataIterator):

    def __init__(self, data_file, train_path, test_path,
                 predictors_size=10, file_endings=".png", name="EyePatchIterator", address="tests",
                 amount_of_eyes=1000, amount_of_patches_each=100):

        self.size = predictors_size

        # a class inheriting from Patch (with a static method .image(predictors))
        self.patch_class = EyePatch

        # List to be filled with EyePatches through update_patch()
        self.patches_ = []
        self.patch_iterator = 0

        # Size of the list is eye_amount*patches_each
        self.amount_of_eyes = amount_of_eyes
        self.amount_of_patches_each = amount_of_patches_each

        self.eye_image_iterator = EyeImageIterator(data_file=data_file, train_path=train_path,
                                                   test_path=test_path, file_endings=file_endings)

        self.update_patches()

        DataIterator.__init__(self, name=name, address=address)

    def next(self):
        self.patch_iterator += 1
        if self.patch_iterator >= len(self.patches_):
            self.update_patches()
            self.patch_iterator = 0
        return self.patches_[self.patch_iterator]

    def update_patches(self):
        Logger.log_detail("EyePatchIterator", "Updating patches")
        self.patches_ = []
        self.patch_iterator = 0
        update_duration = time()

        for i in range(self.amount_of_eyes):
            eye = self.eye_image_iterator.next()

            for j in range(self.amount_of_patches_each):
                self.patches_.append(eye.next_random_patch(size=self.size))

        shuffle(self.patches_)
        update_duration = time() - update_duration
        # Takes 35 seconds for 1000*100 patches
        Logger.log_detail("EyePatchIterator", "Number of patches created:\t%d\n\tUpdate total duration:\t%.2f"
                          % (self.amount_of_eyes*self.amount_of_patches_each, update_duration))
