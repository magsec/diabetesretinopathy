__author__ = 'marcos'
import os
import random
from random import shuffle

import numpy as np
from pandas import read_csv

from src.observations import Eye


class DataHandler:

    def __init__(self, train_file, train_path, test_path):
        self.train_file = train_file
        self.train_path = train_path
        self.test_path = test_path

        self.data = read_csv(train_file)
        random.seed(256)
        # list with names of files
        self.stage_0 = [f + ".jpeg" for f in list(self.data.ix[self.data.iloc[:, 1] == 0, 0])]
        self.stage_1 = [f + ".jpeg" for f in list(self.data.ix[self.data.iloc[:, 1] == 1, 0])]
        self.stage_2 = [f + ".jpeg" for f in list(self.data.ix[self.data.iloc[:, 1] == 2, 0])]
        self.stage_3 = [f + ".jpeg" for f in list(self.data.ix[self.data.iloc[:, 1] == 3, 0])]
        self.stage_4 = [f + ".jpeg" for f in list(self.data.ix[self.data.iloc[:, 1] == 4, 0])]
        self.test = [f for f in os.listdir(test_path) if f.endswith(".jpeg")]

        # list of tuples [(file1,label1),(file2,label2),...]
        self.train, self.validation = None, None
        self.split_train()

        # shuffle them all
        self.shuffle_all()

        # initiate iterator
        self.train_iterator = 0
        self.validation_iterator = 0
        self.test_iterator = 0

        # assert train size equal sum of stages
        assert(len(self.stage_1)+len(self.stage_2)+len(self.stage_3)+len(self.stage_4)+len(self.stage_0)
               == len(self.train)+len(self.validation))
        assert(len(self.train)+len(self.validation) == 35126)
        assert(len(self.test) == 53576)

    def shuffle_all(self):
        shuffle(self.stage_0)
        shuffle(self.stage_1)
        shuffle(self.stage_2)
        shuffle(self.stage_3)
        shuffle(self.stage_4)
        shuffle(self.train)
        shuffle(self.validation)
        shuffle(self.test)

    def split_train(self, prob=0.7):
        '''
        Splits the train data set into train and validation set
        :param prob:
        :return:
        '''
        files = [f + ".jpeg" for f in self.data.iloc[:, 0]]
        labels = [label for label in self.data.iloc[:, 1]]
        # list of tuples
        eyes = zip(files, labels)

        is_train = np.random.binomial(1, prob, len(eyes)) == 1
        eyes = zip(eyes, is_train.tolist())
        # returns train and validation set
        self.train = [eye for (eye, is_train) in eyes if is_train]
        self.validation = [eye for (eye, is_train) in eyes if not is_train]
        shuffle(self.train)
        shuffle(self.validation)

    def next_random_eye(self, stage=None):
        label = None
        if stage == 0:
            eye = random.choice(self.stage_0)
            label = 0
            path = os.path.join(self.train_path, eye)
        elif stage == 1:
            eye = random.choice(self.stage_1)
            label = 1
            path = os.path.join(self.train_path, eye)
        elif stage == 2:
            eye = random.choice(self.stage_2)
            label = 2
            path = os.path.join(self.train_path, eye)
        elif stage == 3:
            eye = random.choice(self.stage_3)
            label = 3
            path = os.path.join(self.train_path, eye)
        elif stage == 4:
            eye = random.choice(self.stage_4)
            label = 4
            path = os.path.join(self.train_path, eye)
        elif stage in ["test", "Test", 5]:
            eye = random.choice(self.test)
            path = os.path.join(self.train_path, eye)
        else:
            eye, label = random.choice(self.train + self.validation)
            path = os.path.join(self.train_path, eye)
        return Eye(path, label)

    def next_train_eye(self):
        if self.train_iterator > len(self.train):
            return None
        else:
            eye, label = self.train[self.train_iterator]
            path = os.path.join(self.train_path, eye)
            self.train_iterator += 1
            return Eye(path, label)

    def next_valid_eye(self):
        if self.validation_iterator > len(self.validation):
            return None
        else:
            eye, label = self.validation[self.validation_iterator]
            path = os.path.join(self.train_path, eye)
            self.validation_iterator += 1
            return Eye(path, label)

    def next_test_eye(self):
        if self.test_iterator > len(self.test):
            return None
        else:
            eye = self.test[self.test_iterator]
            path = os.path.join(self.test_path, eye)
            self.train_iterator += 1
            return Eye(path)


