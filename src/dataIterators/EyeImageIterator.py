__author__ = 'marcos'
import os
import random
from random import shuffle, uniform
from src.abstracts.Observation import Patch
import numpy as np
from pandas import read_csv, read_pickle
from time import time
from src.abstracts.DataIterator import DataIterator
from src.observations.Eye import Eye
from src.tools.Parser import Parser
from src.tools import Logger


class EyeImageIterator(DataIterator):

    def __init__(self, data_file, train_path, test_path, address="tests",
                 file_endings=".jpeg", name="EyeImageIterator"):
        DataIterator.__init__(self, name=name, address=address)
        self.total_train = 35126
        self.total_test = 53576

        self.data_file = data_file
        self.train_path = train_path
        self.test_path = test_path
        self.file_endings = file_endings
        self.data = read_csv(data_file)

        # list with names of files
        self.stage_0 = [f + file_endings for f in list(self.data.ix[self.data.iloc[:, 1] == 0, 0])]
        self.stage_1 = [f + file_endings for f in list(self.data.ix[self.data.iloc[:, 1] == 1, 0])]
        self.stage_2 = [f + file_endings for f in list(self.data.ix[self.data.iloc[:, 1] == 2, 0])]
        self.stage_3 = [f + file_endings for f in list(self.data.ix[self.data.iloc[:, 1] == 3, 0])]
        self.stage_4 = [f + file_endings for f in list(self.data.ix[self.data.iloc[:, 1] == 4, 0])]
        self.test = [f for f in os.listdir(test_path) if f.endswith(file_endings)]
        # list of tuples [(file1,label1),(file2,label2),...]
        self.train = zip(self.stage_0, [0]*len(self.stage_0)) +\
                     zip(self.stage_1, [1]*len(self.stage_1)) +\
                     zip(self.stage_2, [2]*len(self.stage_2)) +\
                     zip(self.stage_3, [3]*len(self.stage_3)) +\
                     zip(self.stage_4, [4]*len(self.stage_4))

        self.stage_0_iterator = 0
        self.stage_1_iterator = 0
        self.stage_2_iterator = 0
        self.stage_3_iterator = 0
        self.stage_4_iterator = 0

        # shuffle them all
        self.shuffle_all()
        self.is_valid()

    def reset(self):
        self.start_time = time()
        self.stage_0_iterator = 0
        self.stage_1_iterator = 0
        self.stage_2_iterator = 0
        self.stage_3_iterator = 0
        self.stage_4_iterator = 0
        self.train_iterator = 0
        self.test_iterator = 0
        self.shuffle_all()
        self.is_valid()

    def is_valid(self):
        # assert train size equal sum of stages
        assert(len(self.stage_1)+len(self.stage_2)+len(self.stage_3)+len(self.stage_4)+len(self.stage_0)
               == len(self.train))
        assert(len(self.train) == self.total_train)
        assert(len(self.test) == self.total_test)

    def shuffle_all(self):
        DataIterator.shuffle_all(self)
        shuffle(self.stage_0)
        shuffle(self.stage_1)
        shuffle(self.stage_2)
        shuffle(self.stage_3)
        shuffle(self.stage_4)

    def exclude_from_train(self, files):
        # WARNING: must change is_valid for the new train_size
        Logger.log_status("%s.%s" % (EyeImageIterator.__name__, EyeImageIterator.exclude_from_train.__name__))
        eye_ids = [Parser.eye_id(file_name) for file_name in files]
        eye_sides = [Parser.eye_side(file_name) for file_name in files]
        files = zip(eye_ids, eye_sides)
        files = [eye + "_" + side + self.file_endings for eye, side in files]
        self.train = [(eye_file, label) for eye_file, label in self.train if eye_file not in files]

    def exclude_from_test(self, files):
        Logger.log_status("%s.%s" % (EyeImageIterator.__name__, EyeImageIterator.exclude_from_test.__name__))
        eye_ids = [Parser.eye_id(file_name) for file_name in files]
        eye_sides = [Parser.eye_side(file_name) for file_name in files]
        files = zip(eye_ids, eye_sides)
        files = [eye + "_" + side + self.file_endings for eye, side in files]
        self.test = [(eye_file, label) for eye_file, label in self.test if eye_file not in files]

    def next(self, eye_class=Eye):
        is_test = uniform(0, 1) > 0.5

        if is_test:
            eye = random.choice(self.test)
            path = os.path.join(self.test_path, eye)
        else:
            eye, label = random.choice(self.train)
            path = os.path.join(self.train_path, eye)
        return eye_class(path, address=self.get_full_address())

    def next_random_observation(self, stage=None, eye_class=Eye, n_patches=1000):
        label = None
        if stage == 0:
            eye = random.choice(self.stage_0)
            label = 0
            path = os.path.join(self.train_path, eye)
        elif stage == 1:
            eye = random.choice(self.stage_1)
            label = 1
            path = os.path.join(self.train_path, eye)
        elif stage == 2:
            eye = random.choice(self.stage_2)
            label = 2
            path = os.path.join(self.train_path, eye)
        elif stage == 3:
            eye = random.choice(self.stage_3)
            label = 3
            path = os.path.join(self.train_path, eye)
        elif stage == 4:
            eye = random.choice(self.stage_4)
            label = 4
            path = os.path.join(self.train_path, eye)
        elif stage in ["test", "Test", 5]:
            eye = random.choice(self.test)
            path = os.path.join(self.test_path, eye)
        else:
            eye, label = random.choice(self.train)
            path = os.path.join(self.train_path, eye)
        return eye_class(path, label=label, address=self.get_full_address(), number_of_patches=n_patches)

    def next_train_observation(self, eye_class=Eye, n_patches=1000):
        '''

        :param eye_class:
        :return: a valid eye_class instance or None if finished
        '''
        if self.train_iterator >= len(self.train):
            return None
        else:
            eye, label = self.train[self.train_iterator]
            path = os.path.join(self.train_path, eye)
            print "Next train observation is ", path
            print "\t%d out of %d Training Eyes" % (self.train_iterator+1, self.total_train)
            print "\tTime passed: %f" % (time() - self.start_time)
            self.train_iterator += 1
            return eye_class(path, label=label, address=self.get_full_address(), number_of_patches=n_patches)

    def next_train_pickle(self, stage=0):
        '''
        Unpickle train file and return object
        :param stage: stage of diabetes
        :return: unpickled object
        '''

        if stage == 0:
            file_name = self.stage_0[self.stage_0_iterator] if self.stage_0_iterator < len(self.stage_0) else None
            self.stage_0_iterator += 1
            past = self.stage_0_iterator
            total = len(self.stage_0)
        elif stage == 1:
            file_name = self.stage_1[self.stage_1_iterator] if self.stage_1_iterator < len(self.stage_1) else None
            self.stage_1_iterator += 1
            past = self.stage_1_iterator
            total = len(self.stage_1)
        elif stage == 2:
            file_name = self.stage_2[self.stage_2_iterator] if self.stage_2_iterator < len(self.stage_2) else None
            self.stage_2_iterator += 1
            past = self.stage_2_iterator
            total = len(self.stage_2)
        elif stage == 3:
            file_name = self.stage_3[self.stage_3_iterator] if self.stage_3_iterator < len(self.stage_3) else None
            self.stage_3_iterator += 1
            past = self.stage_3_iterator
            total = len(self.stage_3)
        elif stage == 4:
            file_name = self.stage_4[self.stage_4_iterator] if self.stage_4_iterator < len(self.stage_4) else None
            self.stage_4_iterator += 1
            past = self.stage_4_iterator
            total = len(self.stage_4)
        else:
            file_name = self.train[self.train_iterator] if self.train_iterator <= len(self.train) else None
            self.train_iterator += 1
            past = self.train_iterator
            total = len(self.train)

        if file_name is None:
            return None
        file_path = os.path.join(self.train_path, file_name)
        obj = read_pickle(file_path)
        patch_id = file_name.split(".")[0]
        print "Next train observation is ", patch_id
        print "\t%d out of %d Training Eyes" % (past, total)
        print "\tTime passed: %f" % (time() - self.start_time)
        return Patch(response=stage, predictors=obj, patch_id=patch_id)

    def next_test_observation(self, eye_class=Eye, n_patches=1000):
        if self.test_iterator >= len(self.test):
            return None
        else:
            eye = self.test[self.test_iterator]
            self.test_iterator += 1
            path = os.path.join(self.test_path, eye)
            print "Next test observation is ", path
            print "\t%d out of %d Test Eyes" % (self.test_iterator, self.total_test)
            print "\tTime passed: %f" % (time() - self.start_time)

            return eye_class(path, address=self.get_full_address(), number_of_patches=n_patches)

    def next_test_pickle(self):
        '''
        Unpickle test file and return object
        :return: unpickled object
        '''
        if self.test_iterator >= len(self.test):
            return None
        else:
            file_name = self.test[self.test_iterator]
            self.test_iterator += 1
            file_path = os.path.join(self.test_path, file_name)
            patch_id = file_name.split(".")[0]
            print "Next test observation is ", patch_id
            print "\t%d out of %d Test Eyes" % (self.test_iterator, self.total_test)
            print "\tTime passed: %f" % (time() - self.start_time)
            obj = read_pickle(file_path)
            return Patch(response=None, predictors=obj, patch_id=patch_id)

