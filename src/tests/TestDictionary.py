__author__ = 'marcos'

import unittest
import numpy as np
from src.machines.Dictionary import Dictionary


class Iterator():
    class Observation():
        def __init__(self, predictors=None):
            self.predictors = predictors

    def __init__(self, dictionary_matrix=None):
        self.dictionary_matrix = dictionary_matrix
        self.activation_size = dictionary_matrix.shape[1]

    def generate_activations(self):
        activations = np.matrix(np.random.laplace(size=self.activation_size)).T
        assert(activations.shape == (self.activation_size, 1))
        return activations

    def next(self, size=None):
        predictor = self.dictionary_matrix*self.generate_activations()
        return Iterator.Observation(predictors=predictor)

    def get_full_address(self):
        return ""


class TestDictionary(unittest.TestCase):

    def setUp(self):
        self.dictionary_matrix = np.matrix(np.array([[1, 2, 3, 4, 5],
                                                    [1, 1, 1, 1, 1],
                                                    [1, 0, 0, 0, 0],
                                                    [1, -1, 1, -1, 0],
                                                    [0, 0, 0, 0, 1],
                                                    [1, 2, 3, 2, 1]]))
        self.iterator = Iterator(self.dictionary_matrix)
        self.predictor_size = self.dictionary_matrix.shape[0]
        self.dictionary_size = self.dictionary_matrix.shape[1]
        self.dictionary = Dictionary(self.iterator, dictionary_size=self.dictionary_size, penalty=0.01)

    def tearDown(self):
        pass

    def test_train(self):
        self.dictionary.train(training_time=1000, batch_size=20)
        print self.dictionary_matrix
        print self.dictionary.dictionary_matrix

    def test_predict(self):
        self.dictionary.dictionary_matrix = self.dictionary_matrix
        for i in range(10):
            activations = self.iterator.generate_activations()
            observation = self.dictionary_matrix*activations
            sparse_code = self.dictionary.predict(observation)

            mean = np.mean(np.abs(activations-sparse_code))
            print mean
            self.assertAlmostEqual(mean, 0, places=0)