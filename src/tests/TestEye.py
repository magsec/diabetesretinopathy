__author__ = 'marcos'

import os
import shutil
import unittest
from skimage.io import imread, imsave
from src.observations.Eye import Eye
from src.abstracts.Abstract import Abstract


class TestEye(unittest.TestCase):

    def setUp(self):
        # creates all necessary objects
        # fixture
        self.name = "99_right"
        self.file_path = '/home/marcos/Universum/Studium/MasterThesis/data/train/'+self.name+".jpeg"
        self.label = 1
        self.address = "testData/TestEye" # will be removed in the end
        self.object = Eye(file_path=self.file_path, label=self.label, address=self.address)

    def tearDown(self):
        if os.path.exists(self.address):
            shutil.rmtree('testData')

    def test_save_and_load(self):
        self.file_name = self.object.save()
        self.assertEqual(self.object.name, self.name)
        self.assertEqual(self.object.address, self.address)

        del self.object
        self.assertEqual(os.path.join(self.address, self.name)+".pkl", self.file_name)
        self.object = Abstract.load(file_name=self.file_name)
        self.assertEqual(self.object.name, self.name)
        self.assertEqual(self.object.address, self.address)

    def test_save_image(self):
        self.image = self.object.image
        file_path1 = self.object.save_image()
        file_path2 = os.path.join(self.address, self.name) + ".png"
        self.assertEqual(file_path1, file_path2, "Image path should have this format.")
        image2 = imread(file_path1)
        self.assertAlmostEqual(image2.mean(), self.image.mean(), msg="Loaded image should be the same as saved one.")

