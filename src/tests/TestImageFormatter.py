__author__ = 'marcos'
import unittest
from skimage import data
from matplotlib import pyplot as plt
import numpy as np
from src.tools.ImageFormatter import ImageFormatter


class TestImageFormatter(unittest.TestCase):

    def setUp(self):
        self.image = data.astronaut()[:, :, 1]

    def tearDown(self):
        pass

    def test_glue_images(self):
        # tests if images are glued correctly, in the right order
        im1 = self.image
        im2 = im1.copy()
        im3 = im1.copy()
        im4 = im1.copy()
        side_y, side_x = im1.shape
        assert(side_y == side_x)

        magic_number = 119
        im1[0, 0] = magic_number
        im2[0:50, :] = magic_number
        im3[:, 0:50] = magic_number
        im4[51:, 51:] = magic_number
        images = np.array([im1.flatten(), im2.flatten(), im3.flatten(), im4.flatten()])

        print images.shape
        image = ImageFormatter.glue_images(images)
        assert(image[0, 0] == magic_number)
        assert(image[0, 2*side_x-1] == magic_number)
        assert(image[2*side_y-1, 0] == magic_number)
        assert(image[-1, -1] == magic_number)

        plt.imshow(image)
        plt.show()