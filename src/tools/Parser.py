__author__ = 'marcos'



class Parser:

    def __init__(self):
        pass

    @staticmethod
    def eye_id(file_name, dir_separator="/"):
        '''
        Extracts the eye id and side from the file name
        :param file_name: path to file, e.g. "marcos/123_left.jpeg"
        :param dir_separator: symbol separating directories, e.g. "/"
        :return: eye_id, eye_side
        '''
        file_name = file_name.split(dir_separator)[-1]
        return file_name.split("_")[0]

    @staticmethod
    def eye_side(file_name, dir_separator="/"):
        '''
        Extracts the eye id and side from the file name
        :param file_name: path to file, e.g. "marcos/123_left.jpeg"
        :param dir_separator: symbol separating directories, e.g. "/"
        :return: eye_id, eye_side
        '''
        file_name = file_name.split(dir_separator)[-1]
        return file_name.split("_")[1].split(".")[0]