__author__ = 'marcos'
import os

TRAIN_DATA = '/home/marcos/Universum/Studium/MasterThesis/data/train/labels.csv'
ORIGINAL_TRAIN = '/home/marcos/Universum/Studium/MasterThesis/data/train'
ORIGINAL_TEST = '/home/marcos/Universum/Studium/MasterThesis/data/test'

GREEN_TRAIN = '/home/marcos/Universum/Studium/MasterThesis/data/green_train'
GREEN_TEST = '/home/marcos/Universum/Studium/MasterThesis/data/green_test'

RESULTS_PATH = "/home/marcos/Universum/Studium/MasterThesis/results"

DIABETES_RESULTS = os.path.join(RESULTS_PATH, "Diabetes")
DIABETES_LIBRARY = os.path.join(DIABETES_RESULTS, "Library")


def eye_path(eye_id, path=GREEN_TRAIN, file_ending=".png"):
    file_path = os.path.join(path, eye_id)
    return file_path + file_ending
