__author__ = 'marcos'
import os
import glob
from math import sqrt
import numpy as np
from numpy import random
# from skimage.transform import rescale
# from skimage.exposure import equalize_adapthist, adjust_gamma
# from skimage.color import rgb2hsv
# from skimage.feature import canny
# from sklearn.decomposition import PCA
from src.tools import Logger


# def main():
#     files = glob.glob(os.path.join(Configs.train_data, "*.*"))
#     for file_path in files:
#         print file_path
#         im = imread(file_path)
#         formatted_image = ImageFormatter(im)
#         plt.imshow(formatted_image.im)
#         plt.show()
#         plt.imshow(im)
#         plt.show()


class ImageFormatter:

    def __init__(self):
        pass

    @staticmethod
    def is_gray(image):
        if image.ndim == 2:
            return True
        else:
            return False

    @staticmethod
    def to_gray(image):
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.to_gray.__name__))
        return np.mean(image, axis=2)

    # @staticmethod
    # def to_value(image):
    #     Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.to_value.__name__))
    #     image = rgb2hsv(image)
    #     return image[:, :, 2]

    @staticmethod
    def nr_non_black_pixels(array, low_intensity=25):
        '''
        Number of pixels with low intensity
        :param array: ndarray
        :return:
        '''
        return (array.flatten() > low_intensity).sum()

    @staticmethod
    def get_eye_circle(image, sigma=5, low=5, high=15):
        return ImageFormatter.get_eye_circle_naive(image)
        image = image if ImageFormatter.is_gray(image) else ImageFormatter.to_gray(image)
        edges = canny(image, sigma=sigma, low_threshold=low, high_threshold=high)

        # get important coordinates
        min_radius = min(image.shape)/3
        center_x = image.shape[1]//2
        center_y = image.shape[0]//2
        y, x = np.where(edges > 0)

        print "Middle x:", center_x
        print "Middle y:", center_y

        # find coordinates that are somewhat distant from center
        distance_from_center = np.power((x-center_x), 2) + np.power((y-center_y), 2)
        indices = np.where(distance_from_center > min_radius**2)
        x = x[indices]
        y = y[indices]

        print len(x)
        print len(y)

        # make linear regression
        response = x**2 + y**2
        assert(len(response) > 10)
        predictors = np.vstack((x, y)).T
        print predictors.shape
        print response.shape

        model = RANSACRegressor(LinearRegression())
        model = model.fit(predictors, response)
        regression = model.estimator_

        print "Coefficients:", regression.coef_
        alpha_x, alpha_y = regression.coef_[0]
        intercept = -regression.intercept_
        center_x = alpha_x//2
        center_y = alpha_y//2

        # WARNING: radius is not very good
        # radius_squared = intercept + center_x**2 + center_y**2
        radius_squared = np.percentile(np.power((x-center_x), 2) + np.power((y-center_y), 2), 70)

    @staticmethod
    def get_eye_circle_naive(image):
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.get_eye_circle.__name__))

        # gray image
        intensities = image if ImageFormatter.is_gray(image) else ImageFormatter.to_gray(image)
        dim_y, dim_x = intensities.shape

        non_black_pixels_vertically = np.apply_along_axis(ImageFormatter.nr_non_black_pixels, 0, intensities)
        non_black_pixels_horizontally = np.apply_along_axis(ImageFormatter.nr_non_black_pixels, 1, intensities)
        non_black_pixels_slice = np.hstack([non_black_pixels_horizontally, non_black_pixels_vertically])

        radius = max(non_black_pixels_slice)//2
        dim_y = image.shape[0]
        dim_x = image.shape[1]
        center_x, center_y = dim_x//2, dim_y//2

        Logger.log_info("Eye center: %f,%f" % (center_x, center_y))
        Logger.log_info("Eye radius: %f" % radius)
        return center_x, center_y, radius

    @staticmethod
    def crop_eye(image, low_intensity=15):
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.crop_eye.__name__))

        gray_image = image if ImageFormatter.is_gray(image) else np.max(image, axis=2)
        dim_y = image.shape[0]
        dim_x = image.shape[1]
        assert(dim_x > 0 and dim_y > 0)

        # Eye lower and upper bounds
        maximum_eye_radius = max(dim_x, dim_y)/2
        minimum_eye_radius = min(dim_x, dim_y)/4
        assert(maximum_eye_radius > 0)
        assert(minimum_eye_radius > 0)
        # minimum_center_x = minimum_eye_radius
        # maximum_center_x = dim_x - minimum_eye_radius
        # minimum_center_y = minimum_eye_radius
        # maximum_center_y = dim_y - minimum_eye_radius

        if low_intensity is None:
            center_y = dim_y//2
            center_x = dim_x//2
            radius = min(center_y, center_x)
            Logger.log_warning("ImageFormatter.crop_eye: Low intensity is None")
            assert(radius < maximum_eye_radius)
            assert(radius > minimum_eye_radius)
        else:
            is_eye = gray_image > low_intensity
            is_eye_y, is_eye_x = np.where(is_eye)

            # makes sure it is not empty
            if not is_eye.any() or is_eye_x.size == 0 or is_eye_y.size == 0:
                if low_intensity == 15:
                    low_intensity = 7
                else:
                    low_intensity = None
                return ImageFormatter.crop_eye(image, low_intensity=low_intensity)

            center_y = int(np.mean(is_eye_y))
            center_x = int(np.mean(is_eye_x))
            diameter_y = max(np.sum(is_eye, axis=0))
            diameter_x = max(np.sum(is_eye, axis=1))
            radius = max(diameter_x, diameter_y)//2

        if radius > maximum_eye_radius:
            if low_intensity == 15:
                low_intensity = 25
            else:
                low_intensity = None
            return ImageFormatter.crop_eye(image, low_intensity=low_intensity)
        elif radius <= minimum_eye_radius:
            if low_intensity == 15:
                low_intensity = 7
            else:
                low_intensity = None
            return ImageFormatter.crop_eye(image, low_intensity=low_intensity)
        else:
            assert(radius > 0)
            margin = 0
            minimum_x = max(0, int(center_x - radius - margin))
            maximum_x = min(dim_x, int(center_x + radius + margin))
            minimum_y = max(0, int(center_y - radius - margin))
            maximum_y = min(dim_y, int(center_y + radius + margin))
            return image[minimum_y:maximum_y, minimum_x:maximum_x]

    @staticmethod
    def crop_eye_complicated(image, margin=0, center_x=None, center_y=None, radius=None):
        '''
        Crops the square containing the given circle with some radius
        :param image: the eye image
        :param margin: extra margin
        :param center_x: center of the eye
        :param center_y: center of the eye
        :param radius: radius of the eye
        :return:
        '''
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.crop_eye.__name__))

        # Compute center and radius of eye
        if center_x is None or center_y is None or radius is None:
            x, y, r = ImageFormatter.get_eye_circle(image)
        center_x = (x if center_x is None else center_x)
        center_y = (y if center_y is None else center_y)
        radius = (r if radius is None else radius)

        dim_y = image.shape[0]
        dim_x = image.shape[1]

        # Find limits of the square to crop
        min_x = max(0, int(center_x - radius - margin))
        max_x = min(dim_x, int(center_x + radius + margin))
        min_y = max(0, int(center_y - radius - margin))
        max_y = min(dim_y, int(center_y + radius + margin))

        Logger.log_info("x range: %d - %d" % (min_x, max_x))
        Logger.log_info("y range: %d - %d" % (min_y, max_y))

        return image[min_y:max_y, min_x:max_x]

    # @staticmethod
    # def resize_image(image, output_size=500):
    #     '''
    #     Scales an image to a side length of the given output size
    #     :param output_size: size of larger side of output image
    #     :return: scaled image
    #     '''
    #     Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.get_eye_circle.__name__))
    #
    #     dim_y = image.shape[0]
    #     dim_x = image.shape[1]
    #     dim = max(dim_x, dim_y)
    #
    #     scale = output_size/float(dim)
    #
    #     Logger.log_info("Original Dimension: %d x %d" % (dim_x, dim_y))
    #     Logger.log_info("Scale Factor: %f" % scale)
    #
    #     return rescale(image, scale)

    # @staticmethod
    # def to_pca_gray(image):
    #     '''
    #
    #     :param image:
    #     :return:
    #     '''
    #     Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.to_pca_gray.__name__))
    #     # image must have three channels
    #     if len(image.shape) != 3:
    #         Logger.log_error("Could not convert image to gray. Maybe it is already gray")
    #         return image
    #
    #     # image infos
    #     dim_y = image.shape[0]
    #     dim_x = image.shape[1]
    #     nr_of_pixels = dim_y*dim_x
    #     nr_of_colors = image.shape[2]
    #
    #     # apply pca
    #     x = image.reshape((nr_of_pixels, nr_of_colors))
    #     pca = PCA(n_components=1)
    #     x = pca.fit_transform(x)
    #     components = pca.components_[0]
    #     Logger.log_info(pca.components_)
    #     # log component values
    #     Logger.log_info("Red: %f" % components[0])
    #     Logger.log_info("Green: %f" % components[1])
    #     Logger.log_info("Blue: %f" % components[2])
    #
    #     # Restructure the image
    #     x = x.reshape((dim_y, dim_x))
    #     return np.floor(x)
    #
    # @staticmethod
    # def adapt_histogram(image):
    #     return equalize_adapthist(image, clip_limit=0.008)
    #
    # @staticmethod
    # def gamma_correction(image, gamma=0.8):
    #     return adjust_gamma(image, gamma=gamma)

    @staticmethod
    def enhance_brightness(image):
        # WARNING: not a good idea
        # macula and retina wont be
        # easily recognisable
        pass

    @staticmethod
    def enhance_contrast(image):
        # might be a good idea
        pass

    @staticmethod
    def enhance_sharpness(image):
        pass

    @staticmethod
    def uniform_image(image):
        '''
        Transform image to have pixel intensities distributed uniformly
        :param image: image
        :return: gray image with pixel intensity uniformly distributed
        '''
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.uniform_image.__name__))

        image = image if image.ndim == 2 else ImageFormatter.to_gray(image)
        # indices of non-black pixels
        non_black_pixel_indices_x, non_black_pixel_indices_y = np.where(image != 0)
        number_of_non_black_pixels = len(non_black_pixel_indices_x)

        # vector with uniformly distributed pixel intensities
        uniform_pixel_intensities = np.linspace(0, 255, num=number_of_non_black_pixels)
        uniform_pixel_intensities = np.floor(uniform_pixel_intensities)

        # find increasing order of pixel intensity in image
        order = np.argsort(image[non_black_pixel_indices_x, non_black_pixel_indices_y])
        # reorder by pixel intensity
        non_black_pixel_indices_x = non_black_pixel_indices_x[order]
        non_black_pixel_indices_y = non_black_pixel_indices_y[order]

        image[non_black_pixel_indices_x, non_black_pixel_indices_y] = uniform_pixel_intensities
        return image

    @staticmethod
    def gaussian_gray(image):
        '''
        Transform image to have pixel intensities distributed uniformly
        :param image: image
        :return: gray image with pixel intensity uniformly distributed
        '''
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.gaussian_gray.__name__))

        image = image if image.ndim == 2 else ImageFormatter.to_gray(image)
        # indices of non-black pixels
        non_black_pixel_indices_x, non_black_pixel_indices_y = np.where(image != 0)
        number_of_non_black_pixels = len(non_black_pixel_indices_x)

        # vector with uniformly distributed pixel intensities
        normal_pixel_intensities = np.sort(random.normal(size=number_of_non_black_pixels))
        normal_pixel_intensities = normal_pixel_intensities - normal_pixel_intensities.min()
        normal_pixel_intensities = 255*normal_pixel_intensities/normal_pixel_intensities.max()
        normal_pixel_intensities = np.floor(normal_pixel_intensities)

        # find increasing order of pixel intensity in image
        order = np.argsort(image[non_black_pixel_indices_x, non_black_pixel_indices_y])
        # reorder by pixel intensity
        non_black_pixel_indices_x = non_black_pixel_indices_x[order]
        non_black_pixel_indices_y = non_black_pixel_indices_y[order]

        image[non_black_pixel_indices_x, non_black_pixel_indices_y] = normal_pixel_intensities
        return image

    @staticmethod
    def gaussian_color(image):
        # WARNING: not a good idea
        # histogram gets concentrated in the middle
        # image gets smooth, looses contrast
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.gaussian_color.__name__))
        image_0 = image[:, :, 0]
        image_1 = image[:, :, 1]
        image_2 = image[:, :, 2]

        image_0 = ImageFormatter.gaussian_gray(image_0)
        image_1 = ImageFormatter.gaussian_gray(image_1)
        image_2 = ImageFormatter.gaussian_gray(image_2)

        result = np.dstack((image_0, image_1, image_2))
        assert(image.shape == result.shape)

        return result

    @staticmethod
    def uniform_pca(image):
        Logger.log_status("%s.%s" % (ImageFormatter.__name__, ImageFormatter.uniform_pca.__name__))
        image_0 = image[:, :, 0]
        image_1 = image[:, :, 1]
        image_2 = image[:, :, 2]

        image_0 = ImageFormatter.uniform_image(image_0)
        image_1 = ImageFormatter.uniform_image(image_1)
        image_2 = ImageFormatter.uniform_image(image_2)

        result = np.dstack((image_0, image_1, image_2))
        assert(image.shape == result.shape)

        return ImageFormatter.to_pca_gray(result)

    @staticmethod
    def project_to_pixel_range(image):
        """
        Shift image into 0-255 range, rescaling if necessary
        :param image: np.ndarray
        :return:
        """
        image -= np.min(image)
        maximum = np.max(image)

        if maximum > 255:
            image /= maximum
        return np.floor(image)


    @staticmethod
    def glue_images(images):
        """

        :param images: an array of images with same size
        :return: an image with images glued in a grid
        """
        number_of_images = images.shape[0]
        if images.ndim == 2:
            # then images are vectorized => reshape them
            image_size = images.shape[1]
            side_length = sqrt(image_size)
            # make sure it is a squared image
            assert(side_length**2 == image_size)
            images = images.reshape((number_of_images, side_length, side_length))

        number_of_horizontal_patches = int(sqrt(number_of_images))
        number_of_vertical_patches = int(number_of_images/number_of_horizontal_patches)
        number_of_displayed_images = number_of_vertical_patches*number_of_horizontal_patches

        # shape = (number_of_displayed_images, side_length, side_length)
        images = images[0:number_of_displayed_images]

        # stacks each image on the side of each other, horizontally
        # creates a strip of images
        images = np.hstack(images)

        # computes strip cutting points
        actual_size_x = images.shape[1]
        final_size_x = actual_size_x/number_of_vertical_patches
        cuts = range(0, actual_size_x, final_size_x)[1:]

        # cuts images strip into smaller strips
        images = np.hsplit(images, cuts)

        # glue image strips vertically
        images = np.vstack(images)

        return images
