__author__ = 'marcos'

status_on = False
info_on = False
detail_on = True
error_on = True
warning_on = True

allowed_classes = ["Dictionary", "EyePatchIterator"]


def log_status(s):
    if status_on:
        print "[STATUS] " + str(s)


def log_info(s):
    if info_on:
        print "[INFO] " + str(s)


def log_detail(class_name, s):
    if detail_on:
        if class_name in allowed_classes:
            print "[DETAIL] " + str(s)


def log_error(e):
    if error_on:
        print "[ERROR] " + str(e)


def log_warning(e):
    if warning_on:
        print "[WARNING]" + str(e)