__author__ = 'marcos'

from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.observations.EyeGreen import EyeGreen
from time import time

train_file = '/home/marcos/Universum/Studium/MasterThesis/data/train/labels.csv'
train_path = '/home/marcos/Universum/Studium/MasterThesis/data/train'
test_path = '/home/marcos/Universum/Studium/MasterThesis/data/test'

green_train = '/home/marcos/Universum/Studium/MasterThesis/data/green_train'
green_test = '/home/marcos/Universum/Studium/MasterThesis/data/green_test'

iterator = EyeImageIterator(train_file, train_path, test_path)
train_size = 35126
test_size = 53576
counter = 0

start = time()
eye = iterator.next_train_observation(eye_class=EyeGreen)
while eye is not None:
    counter += 1
    print "Train:", counter
    print "\t", eye.get_full_address()
    eye.save_image(green_train)
    eye = iterator.next_train_observation(eye_class=EyeGreen)
    if counter > train_size:
        raise IndexError
end = time()
print end-start


counter = 0

start = time()
eye = iterator.next_test_observation(eye_class=EyeGreen)
while eye is not None:
    counter += 1
    print "Test:", counter
    print "\t", eye.get_full_address()
    eye.save_image(green_test)
    eye = iterator.next_test_observation(eye_class=EyeGreen)
    if counter > test_size:
        raise IndexError

end = time()
print end - start