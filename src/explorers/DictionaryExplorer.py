__author__ = 'marcos'
import os
from time import time
from matplotlib import pyplot as plt
from src.tools.Paths import DIABETES_LIBRARY
from src.abstracts.Abstract import Abstract


class DictionaryExplorer(object):

    def __init__(self, library_path):
        self.library_path = library_path
        self.dictionaries = os.listdir(self.library_path)
        self.dictionaries = [dict_names for dict_names in self.dictionaries if dict_names.endswith(".pkl")]
        self.dictionary_ids = range(len(self.dictionaries))
        self.library = zip(self.dictionary_ids, sorted(self.dictionaries))

    def choose_dictionary(self):
        print "Choose one of the following dictionaries: "
        for dict_id, dict_name in self.library:
            print "\t%2d: %s" % (dict_id, dict_name)

        choice = int(raw_input("\nChoice: "))

        if choice not in self.dictionary_ids:
            return None
        else:
            dictionary_name = dict(self.library)[choice]
            print "Dictionary chosen: %s" % dictionary_name

            dictionary_path = os.path.join(DIABETES_LIBRARY, dictionary_name)
            return Abstract.load(dictionary_path)

    @staticmethod
    def choose_plot(dictionary):
        finished_plotting = False
        while not finished_plotting:
            print "0: Plot Dictionary"
            print "1: Plot Statistics"
            print "2: Plot Decomposition"
            print "3: Plot Distribution"
            print "4: Duration per mile"

            choice = raw_input("\nChoose plot: ")

            if choice == "0":
                dictionary.plot_dictionary()
                plt.show()
            elif choice == "1":
                dictionary.plot_statistics()
                plt.show()
            elif choice == "2":
                dictionary.plot_decomposition()
                plt.show()
            elif choice == "3":
                path = dictionary.get_full_address()
                distribution_path = os.path.join(path, "ProbabilityDistribution.pkl")
                if os.path.isfile(distribution_path):
                    distribution = Abstract.load(distribution_path)
                    distribution.show()
                else:
                    "Distribution not found."
            elif choice == "4":
                start = time()
                for i in range(1000):
                    observation = dictionary.next_predictor()
                    dictionary.lasso_encoder.fit(dictionary.dictionary_matrix, observation)
                total = time() - start
                print "Time for 1000 encodings: ", total
            else:
                finished_plotting = True


if __name__ == "__main__":

    explorer = DictionaryExplorer(DIABETES_LIBRARY)
    chosen_dictionary = explorer.choose_dictionary()

    while chosen_dictionary is not None:
        explorer.choose_plot(chosen_dictionary)
        chosen_dictionary = explorer.choose_dictionary()
