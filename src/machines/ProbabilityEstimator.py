__author__ = 'marcos'
import numpy as np
from src.abstracts.Machine import Machine

# TODO: Divide by L1 Norm of sparse code, since it might be a sufficient statistics for something


class ProbabilityEstimator(Machine):

    def __init__(self, data_iterator, granularity=100, name="BayesEstimator", address="test"):
        self.data_iterator = data_iterator
        self.observation_size = self.data_iterator.next().size
        self.labels = self.data_iterator.labels
        self.distribution = np.ones((self.labels.size, self.observation_size, granularity))
        Machine.__init__(self, name=name, address=address)

    def train(self, training_limit=1000000):
        patch = self.data_iterator.next()
        counter = 0

        while patch is not None:
            counter += 1

            label = patch.label
            coordinates, bins = ProbabilityEstimator.to_bins(patch.predictors)
            self.distribution[label, coordinates, bins] += 1
            patch = self.data_iterator.next() if counter < training_limit else None

    def predict(self, observation):
        coordinates, bins = ProbabilityEstimator.to_bins(observation)
        return self.distribution[:, coordinates, bins]

    def to_bins(self, observation):
        '''

        :param observation:
        :return: a tuple of np.ndarray with integers; each of size = self.observations_size
        '''
        # coordinates and bins are valid, i.e. integers inside the correct range
        assert()
        return np.zeros(self.observation_size), np.zeros(self.observation_size)