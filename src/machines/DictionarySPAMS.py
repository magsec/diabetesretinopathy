__author__ = 'marcos'
from src.machines.Dictionary import Dictionary


class DictionarySPAMS(Dictionary):

    def __init__(self, data_iterator, dictionary_size, penalty, name=None, address=None):

        if name is None:
            name = "Dictionary %dx%d - %f penalty" % (self.predictors_size, self.dictionary_size, self.penalty)
        if address is None:
            address = data_iterator.get_full_address()

        Dictionary.__init__(data_iterator=data_iterator, dictionary_size=dictionary_size, penalty=penalty,
                            name=name, address="Tests")