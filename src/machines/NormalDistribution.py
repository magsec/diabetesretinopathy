__author__ = 'marcos'
import numpy as np
from src.abstracts.Machine import Machine


class NormalDistribution(Machine):

    def __init__(self, dimension=64, name="NormalDistribution", address=""):
        self.mean = np.zeros((dimension, 1))
        self.sum = np.zeros((dimension, 1))
        self.cross_prod = np.zeros((dimension, dimension))
        self.inv_cov = np.matrix(np.zeros((dimension, dimension)))
        self.dimension = dimension
        self.n_obs = 0
        self.is_updated = False
        Machine.__init__(self, name=name, address=address)

    def is_valid(self):
        assert(self.mean.shape == (self.dimension, 1))
        assert(self.sum.shape == (self.dimension, 1))
        assert(self.cross_prod.shape == (self.dimension, self.dimension))
        assert(self.inv_cov.shape == (self.dimension, self.dimension))
        assert(isinstance(self.inv_cov, np.matrix))

    def train(self, data_frame):
        self.is_updated = True
        matrix = np.matrix(data_frame)
        self.n_obs += matrix.shape[0]

        self.sum += np.matrix(np.apply_along_axis(np.sum, 0, matrix)).T
        self.cross_prod += matrix.T * matrix
        self.is_valid()

    def features(self, mean, cov):
        '''

        :param mean: a (self.dimension,1) np.matrix
        :param cov: a (self.dimension, self.dimension) np.matrix
        :return: a np.array
        '''
        if not self.is_updated:
            self.update_stats()
        assert(mean.shape == (self.dimension, 1))
        assert(cov.shape == (self.dimension, self.dimension))
        assert(isinstance(mean, np.matrix))
        assert(isinstance(cov, np.matrix))

        # sum-aggregation
        diff = mean - self.mean
        dist = diff.T * self.inv_cov * diff

        # sum-aggregation
        trace = np.sum(np.diag(self.inv_cov * cov))

        # dist is a matrix, trace is a scalar
        return np.array([dist[0, 0], trace])
        # return(dist+trace)

    def update_stats(self):
        cov = self.get_covariance()
        self.inv_cov = np.matrix(np.linalg.inv(cov))
        self.is_updated = True
        self.is_valid()

    def get_covariance(self):
        self.mean = self.sum/float(self.n_obs)

        ave_cross_prod = self.cross_prod/float(self.n_obs)
        mean_cross_prod = self.mean.T * self.mean
        assert(ave_cross_prod.shape == mean_cross_prod.shape)
        return ave_cross_prod - mean_cross_prod


