__author__ = 'marcos'

import numpy as np
from math import sqrt, floor
from time import time
from copy import copy
from matplotlib import pyplot as plt
from pylab import cm

from joblib import Parallel, delayed
from src.abstracts.Machine import Machine
from src.tools import Logger, Statistics
from src.tools.ImageFormatter import ImageFormatter
from sklearn.linear_model import ElasticNet
import matplotlib.gridspec as gridspec
from src.abstracts.Observation import Patch


def _par_predict(encoder, matrix, observation):
    encoder.fit(matrix, observation)
    sparse_code = np.matrix(encoder.coef_).T
    assert(sparse_code.shape == (matrix.shape[1], 1))
    return sparse_code


class Dictionary(Machine):

    def __init__(self, data_iterator, dictionary_size, penalty, name=None, address=None):
        Logger.log_status("Initializing a %s object" % Dictionary.__name__)

        # an iterator with method .next() returning a Patch
        self.data_iterator = data_iterator

        self.predictors_size = self.data_iterator.next().predictors.size
        self.dictionary_size = dictionary_size
        self.penalty = penalty

        if name is None:
            name = "Dictionary %dx%d - %f penalty" % (self.predictors_size, self.dictionary_size, self.penalty)
        if address is None:
            address = data_iterator.get_full_address()

        Logger.log_detail("Dictionary", "Dictionary dimensions: %s x %s" % (self.predictors_size, self.dictionary_size))
        self.dictionary_matrix = np.matrix(np.zeros((self.predictors_size, self.dictionary_size)))
        for i in range(self.dictionary_size):
            self.dictionary_matrix[:, i] = self.next_predictor()

        self.lasso_encoder = ElasticNet(alpha=penalty, l1_ratio=1, fit_intercept=False)

        # Dictionary statistics
        self.percentiles = range(100)
        self.error_l2 = []
        self.absolute_norm_l1 = []
        self.non_zeros_l0 = []
        self.absolute_correlation = []

        self.histogram2d_x = []
        self.histogram2d_y = []

        Machine.__init__(self, name=name, address=address)

    def next_predictor(self):
        predictors = self.data_iterator.next().predictors
        return np.matrix(predictors).T

    def train(self, training_time=4000, batch_size=256):
        Logger.log_detail("Dictionary", "Dictionary is being trained.")
        assert(self.predictors_size == self.dictionary_matrix.shape[0])
        assert(self.dictionary_size == self.dictionary_matrix.shape[1])

        a_matrix = np.matrix(np.zeros((self.dictionary_size, self.dictionary_size)))
        b_matrix = np.matrix(np.zeros((self.predictors_size, self.dictionary_size)))

        total_load_time = []
        total_code_time = []
        computation_time = time()
        for time_step in range(training_time):
            if time_step % 100 == 0:
                Logger.log_detail("Dictionary", "Training step: %d" % time_step)
            # some constants
            theta = batch_size*time_step if time_step < batch_size else batch_size**2 + time_step - batch_size
            beta = (theta + 1 - batch_size)/(theta + 1)

            a_matrix *= beta
            b_matrix *= beta

            for batch_step in range(batch_size):
                load_time = time()
                predictor = self.next_predictor()
                load_time = time() - load_time
                total_load_time.append(load_time)

                coding_time = time()
                sparse_code = self.predict(predictor)
                coding_time = time() - coding_time
                total_code_time.append(coding_time)

                a_matrix += sparse_code*sparse_code.T
                b_matrix += predictor*sparse_code.T

            for column in range(self.dictionary_size):
                dictionary_atom = self.dictionary_matrix[:, column]
                response = b_matrix[:, column]
                prediction = self.dictionary_matrix*a_matrix[:, column]
                diagonal_element = a_matrix[column, column]

                if diagonal_element != 0:
                    dictionary_atom += (response-prediction)/diagonal_element

                projection_norm = max(1, sqrt(np.power(dictionary_atom, 2).sum()))
                self.dictionary_matrix[:, column] = dictionary_atom/projection_norm

        computation_time = time() - computation_time
        load_ratio = 100*np.sum(total_load_time)/computation_time
        coding_ratio = 100*np.sum(total_code_time)/computation_time
        Logger.log_detail("Dictionary",
                          "Training Time:\nDuration:\t%f\nLoading Ratio:\t%.2f%%\nCoding Ratio:\t%.2f%%"
                          % (computation_time, float(load_ratio), float(coding_ratio)))

        self.compute_statistics()

    def par_predict(self, list_of_obs, n_jobs=1):
        '''
        Computes sparse coding for each observation in X
        :param X: a n x predictors_size matrix
        :return: a n x dictionary_size matrix
        '''
        # matrix = copy(self.dictionary_matrix)
        # encoder = copy(self.lasso_encoder)
        list_of_obs = Parallel(n_jobs=n_jobs)(
            delayed(_par_predict)(
                self.lasso_encoder,
                self.dictionary_matrix,
                obs)
            for obs in list_of_obs)
        # list_of_obs = map(self.predict, list_of_obs)
        return [obs.flatten().tolist()[0] for obs in list_of_obs]

    def predict(self, observation):
        """
        Computes the sparse coding of the observation
        :param observation: a vector of shape (predictors_size, 1)
        :return: A np.matrix vector of shape (dictionary_size, 1)
        """
        self.lasso_encoder.fit(self.dictionary_matrix, observation)
        sparse_code = np.matrix(self.lasso_encoder.coef_).T
        assert(sparse_code.shape == (self.dictionary_size, 1))
        return sparse_code

    def predict_patch(self, patch):
        '''
        Computes the patch with the corresponding sparse code
        :param patch: a patch containing the observation to be encoded
        :return: a new patch object
        '''
        self.lasso_encoder.fit(self.dictionary_matrix, patch.predictors)
        sparse_code = np.matrix(self.lasso_encoder.coef_).T
        assert(sparse_code.shape == (self.dictionary_size, 1))
        return Patch(patch.response, predictors=sparse_code)

    def predictors_image(self, predictors, flattened=True):
        """
        Creates an image description of the predictor
        :param predictors:
        :param flattened: should resulting image be flattened to one dimension
        :return:
        """
        image = self.data_iterator.patch_class(predictors=predictors).image()

        if flattened:
            return np.asarray(image).flatten()
        else:
            return np.asarray(image)

    def decomposition_image(self, predictors=None):
        if predictors is None:
            predictors = self.next_predictor()
        sparse_code = self.predict(predictors)

        # get top values of the sparse code, and their positions
        decreasing_index = np.argsort(-np.absolute(sparse_code), axis=0).flatten().tolist()[0]
        size_of_decomposition = min(floor(sqrt(sparse_code.shape[0]))**2, 9)
        top_decreasing_index = decreasing_index[:size_of_decomposition]
        top_sparse_code = sparse_code[top_decreasing_index]

        # creates images for most relevant atoms
        atoms = self.dictionary_matrix[:, top_decreasing_index].T
        images = np.array([self.predictors_image(atom) for atom in atoms])
        assert(images.shape[0] == size_of_decomposition)
        assert(images.shape[1] == self.predictors_size)

        # normalizes
        top_sparse_code_l1_normalized = top_sparse_code/np.sum(np.absolute(top_sparse_code))
        top_sparse_code_l1_normalized = np.asarray(top_sparse_code_l1_normalized.T)[0]

        images = np.multiply(images.T, top_sparse_code_l1_normalized).T
        images = np.array([ImageFormatter.project_to_pixel_range(image) for image in images])

        reconstruction = self.dictionary_matrix*sparse_code
        print "Observation l2-norm:", np.linalg.norm(predictors, ord=2)
        print "Reconstruction l2-norm:", np.linalg.norm(reconstruction, ord=2)
        print "L2 Distance:", np.linalg.norm(predictors-reconstruction, ord=2)

        return ImageFormatter.glue_images(images)

    def dictionary_image(self):
        # (dictionary_size, predictors_size) = multiple images of predictors_size
        images = np.array([self.predictors_image(atom) for atom in self.dictionary_matrix.T])

        assert(images.shape[0] == self.dictionary_size)
        assert(images.shape[1] == self.predictors_size)

        return ImageFormatter.glue_images(images)

    def plot_statistics(self):

        if len(self.percentiles) == len(self.error_l2):
            plt.subplot(2, 2, 1)
            plt.plot(self.percentiles, self.error_l2)
            plt.title("Reconstruction Error")
            plt.xlabel("Percentiles")
            plt.ylabel("L2 Error")

        if len(self.percentiles) == len(self.absolute_norm_l1):
            plt.subplot(2, 2, 2)
            plt.plot(self.percentiles, self.absolute_norm_l1)
            plt.title("Intensity")
            plt.xlabel("Percentiles")
            plt.ylabel("L1 Norm")

        if len(self.percentiles) == len(self.non_zeros_l0):
            plt.subplot(2, 2, 3)
            plt.plot(self.percentiles, self.non_zeros_l0)
            plt.title("Sparsity")
            plt.xlabel("Percentiles")
            plt.ylabel("Number of non-zeros")

        if len(self.percentiles) == len(self.absolute_correlation):
            plt.subplot(2, 2, 4)
            plt.plot(self.percentiles, self.absolute_correlation)
            plt.title("Dependency")
            plt.xlabel("Percentiles")
            plt.ylabel("Correlation")

        # plt.show()

    def plot_decomposition(self, predictors=None):
        Logger.log_detail("Dictionary", "Plotting Decomposition")
        if predictors is None:
            predictors = self.next_predictor()

        gs = gridspec.GridSpec(4, 4)

        plt.subplot(gs[0, 0])
        predictors_image = self.predictors_image(predictors, flattened=False)
        plt.imshow(predictors_image, cmap=cm.gray)
        plt.title("Original Patch")

        plt.subplot(gs[0, 3])
        prediction = self.dictionary_matrix*self.predict(predictors)
        prediction_image = self.predictors_image(prediction, flattened=False)
        plt.imshow(prediction_image, cmap=cm.gray)
        plt.title("Reconstructed Patch")

        plt.subplot(gs[1:, :])
        decomposition_image = self.decomposition_image(predictors=predictors)
        plt.imshow(decomposition_image, cmap=cm.gray)
        plt.title("Top atoms used")
        # plt.show() not call it, in order to use subplot externally

    def plot_dictionary(self):
        Logger.log_detail("Dictionary", "Plotting Dictionary")
        image = self.dictionary_image()
        plt.imshow(image, cmap=cm.gray)
        plt.title("Dictionary for %d dimensions" % self.predictors_size)
        # plt.show()

    def most_used_atoms(self):
        # TODO: use histogram to compute most used atoms
        pass

    def compute_statistics(self, sample_size=1000):
        error_l2 = []
        absolute_norm_l1 = []
        non_zeros_l0 = []

        average = np.zeros((self.dictionary_size, 1))
        covariance = np.zeros((self.dictionary_size, self.dictionary_size))

        for i in range(sample_size):
            observation = self.next_predictor()
            sparse_code = self.predict(observation)
            assert(sparse_code.shape == (self.dictionary_size, 1))
            reconstruction = self.dictionary_matrix*sparse_code

            error_l2.append(np.linalg.norm(reconstruction-observation, ord=2))
            absolute_norm_l1.append(np.linalg.norm(sparse_code, ord=1))
            non_zeros_l0.append((sparse_code != 0).sum())

            average += sparse_code
            covariance += sparse_code*sparse_code.T

        covariance /= sample_size
        average /= sample_size

        covariance -= average*average.T

        variances = np.matrix(np.diag(covariance)).T
        assert(variances.shape == (self.dictionary_size, 1))

        variances = np.sqrt(variances*variances.T)
        variances[variances == 0] = 1
        correlation = covariance/variances
        correlation = correlation.flatten().tolist()[0]

        self.error_l2 = np.percentile(error_l2, self.percentiles)
        self.absolute_norm_l1 = np.percentile(absolute_norm_l1, self.percentiles)
        self.non_zeros_l0 = np.percentile(non_zeros_l0, self.percentiles)
        self.absolute_correlation = np.percentile(np.absolute(correlation), self.percentiles)
