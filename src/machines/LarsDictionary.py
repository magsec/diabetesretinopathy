__author__ = 'marcos'

from sklearn.linear_model import Lars
from src.machines.Dictionary import Dictionary


class LarsDictionary(Dictionary):

    def __init__(self, data_iterator, dictionary_size, sparsity_factor, name=None, address=None):
        self.sparsity_factor = sparsity_factor
        self.predictors_size = data_iterator.next().predictors.size
        self.dictionary_size = dictionary_size

        if name is None:
            name = "LarsDictionary %dx%d - %f sparsity_factor" % \
                   (self.predictors_size, self.dictionary_size, self.sparsity_factor)
        if address is None:
            address = data_iterator.get_full_address()
        Dictionary.__init__(self, data_iterator, dictionary_size, penalty=sparsity_factor, name=name, address=address)

        n_nonzero_coefs = int(sparsity_factor*self.predictors_size)
        self.lasso_encoder = Lars(fit_intercept=False, n_nonzero_coefs=n_nonzero_coefs, fit_path=False)