__author__ = 'marcos'

from src.dataIterators.EyePatchIterator import EyePatchIterator
from src.tools.Paths import DIABETES_LIBRARY
from matplotlib import pyplot as plt
from pylab import cm
from src.machines.Dictionary import Dictionary
from src.machines.FortranDictionary import FortranDictionary
from math import sqrt

data_file = '/home/marcos/Universum/Studium/MasterThesis/data/train/labels.csv'
green_train = '/home/marcos/Universum/Studium/MasterThesis/data/green_train'
green_test = '/home/marcos/Universum/Studium/MasterThesis/data/green_test'


def trip_library():
    pass


def diabetes_library(dictionary_sizes, dictionary_class):
    for PREDICTORS_SIZE, DICTIONARY_SIZE, PENALTY in dictionary_sizes:
        print "*****************", PREDICTORS_SIZE, DICTIONARY_SIZE, PENALTY, "*****************"

        iterator = EyePatchIterator(data_file=data_file, train_path=green_train, test_path=green_test,
                                    predictors_size=PREDICTORS_SIZE, amount_of_eyes=400, amount_of_patches_each=800)

        dictionary = dictionary_class(iterator, dictionary_size=DICTIONARY_SIZE,
                                      penalty=PENALTY, address=DIABETES_LIBRARY)

        # Training with ~1 million patches 4000 x 256
        dictionary.train(training_time=4000, batch_size=256)
        dictionary.save()


def main():
    # dictionary sizes: 200, 400, 800, 1200, 1600
    # penalties: 0.075, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25
    # predictors size: 64, 144, 256
    # TODO: 0.01 does not seem a good penalty for 64
    dictionary_sizes = [(64, 256, 0.0008), (64, 256, 0.001),
                        (64, 512, 0.001), (64, 512, 0.0008),
                        (256, 512, 0.0008), (256, 512, 0.0001)]
    dictionary_class = Dictionary
    diabetes_library(dictionary_sizes, dictionary_class)


if __name__ == "__main__":
    main()