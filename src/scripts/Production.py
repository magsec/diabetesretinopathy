__author__ = 'marcos'

from src.industries.EyeBayesIndustry import EyeBayesIndustry
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.tools.Paths import TRAIN_DATA, GREEN_TRAIN, GREEN_TEST, DIABETES_LIBRARY
from src.explorers.DictionaryExplorer import DictionaryExplorer


def trip_production():
    pass


def eye_production():
    eye_iterator = EyeImageIterator(TRAIN_DATA, GREEN_TRAIN, GREEN_TEST, file_endings=".png")
    dictionary = DictionaryExplorer(DIABETES_LIBRARY).choose_dictionary()
    industry = EyeBayesIndustry(eye_iterator=eye_iterator, dictionary=dictionary)
    industry.produce_test()
    industry.save()
    industry.export_production()





if __name__=="__main__":
    eye_production()