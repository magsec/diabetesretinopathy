__author__ = 'marcos'

import os
import sys
import numpy as np
from pyspark import SparkConf, SparkContext
from src.tools.Paths import DIABETES_LIBRARY
from src.machines.ProbabilityDistribution import diabetes_probability_distribution
from src.explorers.DictionaryExplorer import DictionaryExplorer


def main():
    APP_NAME = "DistributionEstimator"

    conf = SparkConf().setAppName(APP_NAME)
    conf = conf.setMaster("local[1]")
    sc = SparkContext(conf=conf)
    # for package in ZIP_PACKAGES:
    #     print package

    explorer = DictionaryExplorer(DIABETES_LIBRARY)
    dictionary = explorer.choose_dictionary()
    print dictionary.dictionary_size

    distribution = diabetes_probability_distribution(sc, dictionary)
    print np.max(distribution.distribution)
    print np.sum(distribution.distribution)

if __name__ == "__main__":
    main()