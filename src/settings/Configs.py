__author__ = 'marcos'

import os
import glob

base_path = "/home/marcos/Universum/Studium/MasterThesis/Diabetes"

## DATA ##
train_data = os.path.join(base_path, "train")
test_data = os.path.join(base_path, "test")

## RESULTS ##
result_path = os.path.join(base_path, "results")
statistics_result = os.path.join(result_path, "statistics")

# statistic results
train_statistics = os.path.join(statistics_result, "train_statistics.csv")
test_statistics = os.path.join(statistics_result, "test_statistics.csv")