from distutils.core import setup

setup(
    name='Machine',
    version='1.0.0',
    packages=['src', 'src.tests', 'src.tools', 'src.scripts', 'src.machines', 'src.settings', 'src.abstracts',
              'src.explorers', 'src.industries', 'src.statistics', 'src.observations', 'src.dataIterators',
              'src.transformations'],
    url='',
    license='',
    author='marcos',
    author_email='',
    description=''
)
