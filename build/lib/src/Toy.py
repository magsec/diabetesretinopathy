__author__ = 'marcos'
from sklearn.linear_model import ElasticNet
import numpy as np


class Toy():

    def __init__(self):
        self.model = ElasticNet()
        self.X = np.array([[1, 2, 3]])

    def play(self, y):
        self.model.fit(self.X, y)
        return self.model.coef_