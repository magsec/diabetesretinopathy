__author__ = 'marcos'

import numpy as np

from src.tools import Logger
from src.abstracts.Abstract import Abstract


class Patch(object):

    def __init__(self, response=None, predictors=None):
        assert isinstance(predictors, np.ndarray)
        self.predictors = predictors
        self.response = response

    @staticmethod
    def image(predictors):
        """
        Creates an np.ndarray with an image of the predictors
        :param predictors:
        :return:
        """
        pass


class Observation(Abstract):

    def __init__(self, file_path, name=None, address=None, label=None):

        Logger.log_status("Initializing a %s object" % Observation.__name__)
        Abstract.__init__(self, name, address)

        # data block
        self.file_path = file_path
        self.response = label
        self.prediction = None

        # patches
        self.patch_iterator = 0
        self.patch_positions = []

    def update_patch_positions(self):
        self.patch_positions = []

    def next_patch(self, size=10):
        '''
        Returns the next observation patch or None.
        :return:
        '''
        self.patch_iterator += 1
        return np.array([0, 0, 0])

    def next_random_patch(self, size=10):
        return np.array([0, 0, 0])