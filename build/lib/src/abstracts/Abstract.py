__author__ = 'marcos'
import os
import pickle
from time import time


class Logger:
    def __init__(self, title, description=None, notes=None):
        self.title = title
        self.description = description
        self.notes = notes


class Abstract(object):

    def __init__(self, name="Abstract", address="tests"):
        self.start_time = time()
        self.name = name
        self.address = address
        self.loggers = []
        self.log("__init__", "Object created.")

    def log(self, title, description=None, notes=None):
        '''
        Logs the modifications done on the object
        :return:
        '''
        self.loggers.append(Logger(title, description, notes))

    def description(self):
        '''
        Prints description of object
        :return: None
        '''
        index_logger = zip(range(len(self.loggers)), self.loggers)
        for i, logger in index_logger:
            print i, "-", logger.title
            print "\t", logger.description
            print "\t", logger.notes
            print "\n"

    def get_full_address(self):
        '''
        Returns the address of the object
        :return:
        '''
        return os.path.join(self.address, self.name)

    def update(self, abstract):
        '''
        Changes the path according to abstracts object
        :return:
        '''
        self.address = abstract.get_full_address()


    def save(self):
        '''
        Saves the object.
        :return:
        '''
        path = self.address
        if not os.path.exists(path=path):
            os.makedirs(path)
        file_path = self.get_full_address()+".pkl"
        output = open(file_path, "wb")
        pickle.dump(self, output)
        output.close()
        return file_path

    @staticmethod
    def load(file_name):
        '''
        Loads an abstract object from file
        :param file_name:
        :return:
        '''
        pkl_file = open(file_name, "rb")
        obj = pickle.load(pkl_file)
        pkl_file.close()
        return obj

