__author__ = 'marcos'
from src.abstracts.Abstract import Abstract


class Industry(Abstract):

    def __init__(self, name="Industry", address="tests"):
        Abstract.__init__(self, name=name, address=address)
        # dictionary: key=id, value=patch object
        self.product = {}

    def produce_test(self):
        '''
        Produces the predictions
        :return:
        '''
        pass

    def export_production(self):
        '''
        Export the predictions to a data file
        :return:
        '''
        pass