__author__ = 'marcos'
from src.abstracts.Abstract import Abstract


class Machine(Abstract):

    def __init__(self, name=None, address=None):
        if name is None:
            name = "Machine"
        if address is None:
            address = "~/.test"
        Abstract.__init__(self, name, address)

    def train(self, data_iterator):
        '''
        Iterates over data, training the machine.
        :param data_iterator: an object with method next()
        :return:
        '''
        pass

    # TODO: why do we need this?
    # def transform(self, observation):
    #     '''
    #     Transforms observation
    #     :param observation:
    #     :return:
    #     '''
    #     pass

    def predict(self, observation):
        '''
        Predicts an object for the observation
        :param observation:
        :return:
        '''
        pass