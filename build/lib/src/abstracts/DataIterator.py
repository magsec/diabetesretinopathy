__author__ = 'marcos'
from src.abstracts.Abstract import Abstract
from src.abstracts.Observation import Observation

from random import shuffle


class DataIterator(Abstract):

    def __init__(self, name="DataIterator", address="tests"):
        Abstract.__init__(self, name, address)

        self.train = []
        self.test = []

        # initiate iterator
        self.train_iterator = 0
        self.validation_iterator = 0
        self.test_iterator = 0

    def shuffle_all(self):
        shuffle(self.train)
        shuffle(self.test)

    def next(self):
        '''
        Returns the next observation patch.
        :return: numpy.array
        '''
        # TODO Warning: returns only eyes from the train set; does not include test eyes
        return Observation("")

    def next_random_observation(self):
        return Observation("")

    def next_train_observation(self):
        return Observation("")

    def next_validation_observation(self):
        return Observation("")

    def next_test_observation(self):
        return Observation("")
