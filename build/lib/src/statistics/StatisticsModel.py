__author__ = 'marcos'

from random import seed

import pandas as pd
import numpy as np
from pandas import read_csv
from pandas import get_dummies
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from skll.metrics import kappa

from src.settings import Configs
from src.tools.Logger import log_status


def main():
    SEED = 23

    train_data_path = Configs.train_statistics
    test_data_path = Configs.test_statistics

    train_data = read_csv(train_data_path)
    test_data = read_csv(test_data_path)

    model = StatisticsModel(train_data)


    seed(SEED)
    train_score = model.cross_validate(n_folds=10)

    # Scores by cross validation
    print "Random Forest Classifier"
    print "Scores: "
    for score in train_score:
        print "\t" + str(score)
    print "Mean Score: " + str(train_score.mean())

    seed(SEED)
    model.train_model()
    solution = model.predict(test_data)
    print model.model.get_params()

    # model_path = os.path.join(test_data_path.split(".")[0], 'model')
    # if not os.path.exists(model_path):
    #     os.makedirs(model_path)
    #
    # submission_file = os.path.join(model_path, "submission.csv")
    # solution.to_csv(submission_file, index=False)


class StatisticsModel:

    def __init__(self, train_data):
        self.model = None
        StatisticsModel.pre_process(train_data)
        self.train_X = train_data.drop('label', 1)
        self.train_y = train_data['label']


    @staticmethod
    def pre_process(data):
        '''
        Make necessary pre-processing to the data.
            - transforms binary categorical data into 0-1 integers
        :param data: DataFrame
        :return: None
        '''

        is_categorical_column = data.applymap(lambda x: isinstance(x, str)).apply(any, 0).values
        categorical_data = data.loc[:, is_categorical_column]
        categorical_data = categorical_data.apply(lambda x: get_dummies(x).iloc[:, 0], 0)
        data.loc[:, is_categorical_column] = categorical_data

        # compute radius ratio

    def train_model(self, model=RandomForestClassifier):
        self.model = model()
        self.model.fit(self.train_X, self.train_y)

    def cross_validate(self, model=RandomForestClassifier, n_folds=3):
        '''
        Trains random forest model
        :return:
        '''
        log_status("StatisticsModel.train_model")

        self.model = model()

        nr_train_obs = len(self.train_X)
        kf = KFold(nr_train_obs, n_folds=n_folds)
        results = np.ndarray(0)

        for train_index, test_index in kf:
            self.model.fit(self.train_X.iloc[train_index], self.train_y.iloc[train_index])
            y_predictions = self.model.predict(self.train_X.iloc[test_index])
            results = np.append(results, kappa(self.train_y.iloc[test_index], y_predictions))

        return results

    def predict(self, test_data):
        sides = test_data['side']
        StatisticsModel.pre_process(test_data)
        test_X = test_data.drop('label', 1)

        if self.model is None:
            raise Exception("No model has been trained.")

        predictions = self.model.predict(test_X)
        labels = [str(identity) + "_" + str(side) for identity, side in zip(test_data['id'], sides)]

        return pd.DataFrame({'image': labels, 'level': predictions})








if __name__ == '__main__':
    main()

