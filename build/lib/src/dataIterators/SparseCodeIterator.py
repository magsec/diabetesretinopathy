__author__ = 'marcos'
from src.abstracts.DataIterator import DataIterator
from src.abstracts.Observation import Patch


class SparseCodeIterator(DataIterator):

    def __init__(self, data_iterator, dictionary, name="SparseCodeIterator", address="tests"):
        self.data_iterator = data_iterator
        self.dictionary = dictionary
        DataIterator.__init__(self, name=name, address=address)

    def next(self):
        # TODO: use SPARK instead
        observation = self.data_iterator.next_patch()
        label = self.data_iterator
        predictors = self.dictionary.predict(observation)
        Patch(response=label, predictors=predictors)