__author__ = 'marcos'
import os
from src.abstracts.DataIterator import DataIterator


class DriverIterator(DataIterator):

    def __init__(self, data_dir, name="DriverIterator", address="test"):
        DataIterator.__init__(self, name, address)
        drivers = os.listdir(data_dir)
        self.iterator = 0

    def next(self):
        # todo
        pass

    def next_random_observation(self):
        # return a random driver
        pass

    def next_train_observation(self):
        self.next()

    def next_test_observation(self):
        self.next()