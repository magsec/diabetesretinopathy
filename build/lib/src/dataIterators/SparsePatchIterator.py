__author__ = 'marcos'
from src.abstracts.DataIterator import DataIterator
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.tools import Logger
from time import time
import numpy as np
from src.abstracts.Observation import Patch


class SparsePatchIterator(DataIterator):

    def __init__(self, dictionary, data_file, train_path, test_path,
                 file_endings=".png", name="SparseCodeIterator", address=None, train_size=50000):
        self.start_time = time()
        self.train_count = 0
        self.train_size = train_size
        self.train_label = 0

        self.eye_iterator = EyeImageIterator(data_file, train_path=train_path, test_path=test_path,
                                             file_endings=file_endings)
        self.train_eye = None
        self.test_eye = None

        self.dictionary = dictionary
        self.predictors_size = self.dictionary.dictionary_size

        if address is None:
            address = self.dictionary.get_full_address()
        DataIterator.__init__(self, name=name, address=address)

    def next_train_eye(self, n_patches=1000):
        '''
        Returns next random eye used for training, making sure we get a balanced amount of eye_labels
        :return: an eye object or None if finished
        '''
        # advance label iterator
        self.train_label += 1
        self.train_label %= 5
        self.train_count += 1

        if self.train_count > self.train_size:
            return None
        else:
            print "\t%d out of %d Training Eyes" % (self.train_count, self.train_size)
            print "\tTime passed: %f" % (time() - self.start_time)
            Logger.log_detail("SparsePatchIterator", "Next training eye of label %d" % self.train_label)
            return self.eye_iterator.next_random_observation(stage=self.train_label, n_patches=n_patches)

    def next_train_observation(self, n_patches=1000):
        if self.train_eye is None:
            self.train_eye = self.next_train_eye(n_patches=n_patches)

        if self.train_eye is None:
            # All train eyes have been iterated
            return None
        else:
            eye_patch = self.train_eye.next_patch(size=self.predictors_size)

        # make sure eye_patch wont be None
        while eye_patch is None:
            # if eye_patch is None,
            # then train_eye has already iterated over all its patches
            # get next eye
            self.train_eye = self.next_train_eye(n_patches=n_patches)

            if self.train_eye is None:
                # If train_eye is None,
                # all train eyes have been iterated
                return None
            else:
                eye_patch = self.train_eye.next_patch(size=self.predictors_size)

        # eye_patch is not None and it is valid by contract
        # encode the eye_patch
        return self.dictionary.predict_patch(eye_patch)

    def train_list(self, n_obs, n_patches=1000):
        list_of_obs = [self.next_train_observation(n_patches=n_patches) for _ in range(n_obs)]
        list_of_obs = [element for element in list_of_obs if element is not None]
        return list_of_obs

    def next_test_observation(self):
        if self.test_eye is None:
            self.test_eye = self.eye_iterator.next_test_observation()

        if self.test_eye is None:
            # All test eyes have been iterated
            return None
        else:
            eye_patch = self.test_eye.next_patch(size=self.predictors_size)

        while eye_patch is None:
            # test_eye has already iterated over all its patches

            self.test_eye = self.eye_iterator.next_test_observation()

            if self.test_eye is None:
                # All test eyes have been iterated
                return None
            else:
                eye_patch = self.test_eye.next_patch(size=self.predictors_size)

        # eye_patch is not None and it is valid by contract
        # encode the eye_patch
        return self.dictionary.predict_patch(eye_patch)

