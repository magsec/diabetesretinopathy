__author__ = 'marcos'

import os
import numpy as np

from matplotlib import pyplot as plt
from pylab import cm
from math import sqrt, floor

from skimage.io import imread, imsave

from src.abstracts.Observation import Observation, Patch
from src.tools.ImageFormatter import ImageFormatter
from src.tools.Parser import Parser
from src.tools import Logger, Statistics


class EyePatch(Patch):

    def __init__(self, predictors=None, response=None, eye_object=None, position=None, size=10):
        # predictors should have squared size
        self.is_valid = True

        if predictors is None:
            y_coordinates, x_coordinates = EyePatch.squared_patch(size=size)
            y_coordinates += position[0]
            x_coordinates += position[1]

            if np.min(x_coordinates) < 0:
                self.is_valid = False
            if np.min(y_coordinates) < 0:
                self.is_valid = False
            if np.max(x_coordinates) >= eye_object.size_x:
                self.is_valid = False
            if np.max(y_coordinates) >= eye_object.size_y:
                self.is_valid = False

            if self.is_valid:
                predictors = eye_object.image[y_coordinates, x_coordinates]
                predictors = Statistics.normalize(predictors)
            else:
                predictors = np.array([])
            response = eye_object.response

        # predictors should be represented as a squared patch
        assert(floor(sqrt(predictors.size))**2 == predictors.size)
        Patch.__init__(self, response=response, predictors=predictors)

    @staticmethod
    def squared_patch(size=10):
        # creates squared patch
        side_length = int(sqrt(size))
        side = np.arange(side_length) - side_length//2
        assert(side_length > 0)
        y_coordinates, x_coordinates = np.meshgrid(side, side)
        return y_coordinates.flatten(), x_coordinates.flatten()

    def image(self):
        '''
        Formats the predictors into an image np.ndarray
        :return:
        '''

        size = self.predictors.size
        side_length = int(sqrt(size))
        assert(size != 0)
        assert(side_length**2 == size)

        predictors = self.predictors - np.min(self.predictors)
        predictors /= np.max(predictors)
        predictors *= 255
        predictors = np.floor(predictors)
        predictors = np.reshape(predictors, (side_length, side_length))
        return predictors

    def plot(self):
        plt.imshow(self.image(), cmap=cm.gray)
        plt.show()

