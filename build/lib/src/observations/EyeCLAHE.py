__author__ = 'marcos'

from src.observations.Eye import Eye
from src.tools.ImageFormatter import ImageFormatter
from src.tools import Logger


class EyeCLAHE(Eye):

    def __init__(self, file_path, label=None, address=None):

        Logger.log_status("Initializing a %s object" % EyeCLAHE.__name__)
        Eye.__init__(self, file_path, label=label, address=address)

    def process_image(self):
        '''
        Make all necessary image processing steps:
        crops eye, adjust brightness and contrasts,
        make the image gray
        :return:
        '''
        Logger.log_status("%s.%s" % (EyeCLAHE.__name__, EyeCLAHE.process_image.__name__))

        self.image = ImageFormatter.crop_eye(self.image)
        self.image = ImageFormatter.adapt_histogram(self.image)
        self.image = ImageFormatter.resize_image(self.image)
        self.image = ImageFormatter.to_gray(self.image)



