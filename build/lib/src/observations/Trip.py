__author__ = 'marcos'
import random
from src.abstracts.Observation import Observation, Patch
from src.tools.TripFormatter import TripFormatter


class TripPatch(Patch):

    def __init__(self, trip_id, response, predictors):
        Patch.__init__(self, response=response, predictors=predictors)
        self.trip_id = trip_id
        assert(self.predictors.shape == (self.predictors.size, 1))


class Trip(Observation):

    def __init__(self, file_path, driver, trip_id, trip, name, address):
        Observation.__init__(self, file_path=file_path, name=name, address=address, label=driver)
        self.trip_id = trip_id
        self.predictors = trip if trip is not None else TripFormatter.get_trip(file_path)
        self.response = driver
        assert(self.predictors.shape == (trip.size, 1))

    def next_patch(self, size=20):
        if self.patch_iterator > self.predictors.size - size:
            return None
        else:
            start = self.patch_iterator
            end = start + size - 1
            predictors = self.predictors[start:end]
            predictors = TripFormatter.format_trip_patch(predictors)
            self.patch_iterator += 2
            return TripPatch(trip_id=self.trip_id, response=self.response, predictors=predictors)

    def next_random_patch(self, size=20):
        starts = self.predictors.size - size
        start = random.randint(0, starts)
        end = start + size + size - 1
        predictors = self.predictors[start:end]
        predictors = TripFormatter.format_trip_patch(predictors)
        return TripPatch(trip_id=self.trip_id, response=self.response, predictors=predictors)

    def show(self):
        '''
        Plots the trip
        :return:
        '''
        pass






