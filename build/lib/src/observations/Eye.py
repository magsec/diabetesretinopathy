__author__ = 'marcos'

import os
import numpy as np

from matplotlib import pyplot as plt
from pylab import cm
from math import sqrt
from random import randint, shuffle

from skimage.io import imread, imsave

from src.abstracts.Observation import Observation
from src.observations.EyePatch import EyePatch
from src.tools.ImageFormatter import ImageFormatter
from src.tools.Parser import Parser
from src.tools import Logger, Statistics


class Eye(Observation):

    def __init__(self, file_path, label=None, address="tests", number_of_patches=1000):

        Logger.log_status("Initializing a %s object" % Eye.__name__)
        self.number_of_patches = number_of_patches

        self.eye_id = Parser.eye_id(file_path)
        self.eye_side = Parser.eye_side(file_path)
        self.image = imread(file_path)

        self.name = self.eye_id + "_" + self.eye_side

        self.process_image()
        # image statistics
        self.size = self.image.size
        self.size_y = self.image.shape[0]
        self.size_x = self.image.shape[1]
        self.center_x = self.size_x//2
        self.center_y = self.size_y//2
        self.radius = max(self.center_x, self.center_y)
        self.is_gray = ImageFormatter.is_gray(self.image)
        # self.eye_area = ImageFormatter.nr_non_black_pixels(self.image, low_intensity=10)

        Observation.__init__(self, file_path, label=label, name=self.name, address=address)

    def update_statistics(self):
        self.size_y = self.image.shape[0]
        self.size_x = self.image.shape[1]
        self.center_x = self.size_x//2
        self.center_y = self.size_y//2
        self.radius = max(self.center_x, self.center_y)
        self.size = self.image.size
        self.is_gray = ImageFormatter.is_gray(self.image)
        # self.eye_area = ImageFormatter.nr_non_black_pixels(self.image, low_intensity=10)

    def get_patch_position(self):
        # return a list with tuples of points in the circle
        self.update_statistics()
        pixel_y_coordinates, pixel_x_coordinates = np.where(self.image > -1)
        pixel_coordinates = zip(pixel_y_coordinates.tolist(), pixel_x_coordinates.tolist())
        return [(y, x) for y, x in pixel_coordinates if (y-self.center_y)**2 + (x-self.center_x)**2 <= self.radius**2]

    def update_patch_positions(self):
        # updates list of self.patch_positions
        self.update_statistics()
        pixel_y_coordinates, pixel_x_coordinates = np.where(self.image > -1)
        pixel_coordinates = zip(pixel_y_coordinates.tolist(), pixel_x_coordinates.tolist())
        self.patch_positions = [(y, x) for y, x in pixel_coordinates
                                if (y-self.center_y)**2 + (x-self.center_x)**2 <= self.radius**2]
        # select 1000 at random
        if self.number_of_patches is not None and self.number_of_patches < len(self.patch_positions):
            shuffle(self.patch_positions)
            self.patch_positions = self.patch_positions[0:self.number_of_patches]

    def next_patch(self, size=10):
        '''

        :param size: predictors_size
        :return: Next valid eye_patch or None if finished.
        '''

        # lazy loading of patches
        if len(self.patch_positions) == 0:
            self.update_patch_positions()

        if self.patch_iterator >= len(self.patch_positions):
            return None

        position = self.patch_positions[self.patch_iterator]
        self.patch_iterator += 1
        patch = EyePatch(eye_object=self, position=position, size=size)

        while not patch.is_valid:
            if self.patch_iterator >= len(self.patch_positions):
                return None
            position = self.patch_positions[self.patch_iterator]
            self.patch_iterator += 1
            patch = EyePatch(eye_object=self, position=position, size=size)

        return patch

    def next_random_patch(self, size=10):
        # lazy loading of patches
        if len(self.patch_positions) == 0:
            self.update_patch_positions()

        limit_counter = 0
        limit = 100
        last_index = len(self.patch_positions) - 1

        patch_iterator = randint(0, last_index)
        limit_counter += 1

        position = self.patch_positions[patch_iterator]
        patch = EyePatch(eye_object=self, position=position, size=size)

        while not patch.is_valid:
            if limit_counter > limit:
                return None

            patch_iterator = randint(0, last_index)
            limit_counter += 1

            position = self.patch_positions[patch_iterator]
            patch = EyePatch(eye_object=self, position=position, size=size)

        return patch

    def patches(self, size=64):
        # lazy loading of patches
        if len(self.patch_positions) == 0:
            self.update_patch_positions()

        results = []
        for position in self.patch_positions:
            patch = EyePatch(eye_object=self, position=position, size=size)
            if patch.is_valid:
                results.append(patch)

        return results

    def transform_to(self, eye_class, **kwargs):
        if eye_class is None:
            return None
        else:
            return eye_class(file_path=self.file_path, **kwargs)

    def process_image(self):
        '''
        Make all necessary image processing steps:
        crops eye, adjust brightness and contrasts,
        make the image gray
        :return:
        '''
        Logger.log_status("%s.%s" % (Eye.__name__, Eye.process_image.__name__))

    def save_image(self, path=None):
        if path is None:
            path = self.address
        if not os.path.exists(path=path):
            os.makedirs(path)
        file_name = os.path.join(path, self.name)+".png"
        imsave(file_name, self.image)
        return file_name

    def show(self):
        dimensions = self.image.ndim
        if dimensions == 2:
            plt.imshow(self.image, cmap=cm.gray)
            # plt.show()
        elif dimensions == 3:
            plt.imshow(self.image)
            # plt.show()
        else:
            raise ValueError("Image is not valid. It should have either 2 or 3 dimensions")





