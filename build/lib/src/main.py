__author__ = 'marcos'
import os
from pyspark import SparkConf, SparkContext
from time import time
import numpy as np
from src.abstracts.Abstract import Abstract
from src.explorers.DictionaryExplorer import DictionaryExplorer, DIABETES_LIBRARY
from glmnet.elastic_net import ElasticNet
from glmnet.glmnet import GlmnetLinearModel


explorer = DictionaryExplorer(DIABETES_LIBRARY)
dictionary = explorer.choose_dictionary()

print "Starting"

total = 0
start = time()
for i in range(1000):
    observation = dictionary.next_predictor()
    dictionary.lasso_encoder.fit(dictionary.dictionary_matrix, observation)
    total += np.sum(dictionary.lasso_encoder.coef_ != 0)
total1 = time() - start
print "Average Non-Zero", total/1000


lasso_encoder = ElasticNet(alpha=1, rho=0.04)
start = time()
total = 0
for i in range(1000):
    observation = dictionary.next_predictor()
    lasso_encoder.fit(dictionary.dictionary_matrix, observation)
    total += np.sum(lasso_encoder.coef_ != 0)
total2 = time() - start
print "Average Non-Zero", total/1000


print "Time for 1000 patches: ", total1
print "Time for 1000 patches: ", total2