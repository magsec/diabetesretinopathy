__author__ = 'marcos'

from matplotlib import pyplot as plt
from random import seed
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.observations.EyeCLAHE import EyeCLAHE
from src.observations.EyeUniformPCA import EyeUniformPCA
from src.observations.EyePCA import EyePCA
from src.observations.EyeGreen import EyeGreen
from src.observations.EyeOther import EyeOther

original = True

train_file = '/home/marcos/Universum/Studium/MasterThesis/data/train/labels.csv'
original_train = '/home/marcos/Universum/Studium/MasterThesis/data/train'
original_test = '/home/marcos/Universum/Studium/MasterThesis/data/test'

green_train = '/home/marcos/Universum/Studium/MasterThesis/data/green_train'
green_test = '/home/marcos/Universum/Studium/MasterThesis/data/green_test'

if original:
    file_endings = ".jpeg"
    train_path = original_train
    test_path = original_test
else:
    file_endings = ".png"
    train_path = green_train
    test_path = green_test

iterator = EyeImageIterator(train_file, train_path, test_path, file_endings=file_endings)
seed(256)
finished = False
while not finished:
    print "Choose next eye: "
    print "\t\t0:\tNo diabetes"
    print "\t\t1:\tMild DR"
    print "\t\t2:\tModerate DR"
    print "\t\t3:\tSevere DR"
    print "\t\t4:\tProliferative DR"
    print "\t\t5:\tTest Data"
    print "\t\t6:\tTrain Data"

    choice = raw_input("\nChoose an option: ")

    if choice == "0":
        eye = iterator.next_random_observation(stage=0)
    elif choice == "1":
        eye = iterator.next_random_observation(stage=1)
    elif choice == "2":
        eye = iterator.next_random_observation(stage=2)
    elif choice == "3":
        eye = iterator.next_random_observation(stage=3)
    elif choice == "4":
        eye = iterator.next_random_observation(stage=4)
    elif choice == "5":
        eye = iterator.next_random_observation(stage=5)
    elif choice == "6":
        eye = iterator.next_random_observation(stage=6)
    else:
        finished = True

    if not finished:
        if original:
            plt.subplot(2, 2, 1)
            eye.show()

            plt.subplot(2, 2, 2)
            eye_clahe = eye.transform_to(eye_class=EyePCA)
            eye_clahe.show()

            plt.subplot(2, 2, 3)
            eye_uniform_pca = eye.transform_to(eye_class=EyeGreen)
            eye_uniform_pca.show()

            plt.subplot(2, 2, 4)
            eye_other = eye.transform_to(eye_class=EyeOther)
            eye_other.show()

            plt.show()
        else:
            eye.show()
            plt.show()


