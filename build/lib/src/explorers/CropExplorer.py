__author__ = 'marcos'

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from random import seed
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.tools.ImageFormatter import ImageFormatter
from skimage.feature import canny
from sklearn.linear_model import RANSACRegressor, LinearRegression
from math import sqrt


train_file = '/home/marcos/Universum/Studium/MasterThesis/data/train/labels.csv'
train_path = '/home/marcos/Universum/Studium/MasterThesis/data/train'
test_path = '/home/marcos/Universum/Studium/MasterThesis/data/test'

iterator = EyeImageIterator(train_file, train_path, test_path)

sigma = 5#int(raw_input("\nGaussian Filter Sigma:"))
low = 10#int(raw_input("\nLow Threshold for Canny edge:"))
high = 20#int(raw_input("\nHigh Threshold for Canny Edge:"))

finished = False
i = 0
while not finished:
    i += 1
    eye = iterator.next_random_observation()
    print i, eye.get_full_address()
    image = eye.image
    image_copy = image.copy()
    image = np.max(image, axis=2)

    is_eye = image > 15
    positions_y, positions_x = np.where(is_eye)
    center_y = int(np.mean(positions_y))
    center_x = int(np.mean(positions_x))
    diameter_y = max(np.sum(is_eye, axis=0))
    diameter_x = max(np.sum(is_eye, axis=1))
    radius = max(diameter_x, diameter_y)//2

    minimum_x = np.min(positions_x)
    maximum_x = np.max(positions_x)
    minimum_y = np.min(positions_y)
    maximum_y = np.max(positions_y)

    is_eye[minimum_y:maximum_y, minimum_x:(minimum_x+10)] = 255
    is_eye[minimum_y:maximum_y, maximum_x:(maximum_x+10)] = 255
    plt.imshow(is_eye, cmap=cm.Greys_r)
    plt.show()

    # image_copy = ImageFormatter.crop_eye(image_copy)
    # plt.imshow(image_copy)
    # plt.show()



