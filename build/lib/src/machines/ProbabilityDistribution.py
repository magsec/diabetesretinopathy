__author__ = 'marcos'
import os
import sys
from math import log, floor
from copy import copy
import numpy as np
from matplotlib import pyplot as plt

from src.abstracts.Machine import Machine
from src.abstracts.Abstract import Abstract
from src.abstracts.Observation import Patch
from src.tools.Paths import TRAIN_DATA
from src.tools import Paths
from src.observations.Eye import Eye
from src.observations.EyePatch import EyePatch


class ProbabilityDistribution(Machine):

    ONE_OFF = 1

    # TODO: add data_iterator
    def __init__(self, number_of_labels, predictors_size, granularity=100,
                 name="ProbabilityDistribution", address="test", average_l1=4):
        self.predictors_size = predictors_size
        self.number_of_labels = number_of_labels
        self.granularity = granularity
        # used to normalize distribution with sigmoid
        self.average_l1 = average_l1
        self.distribution = np.ones((self.number_of_labels, self.predictors_size, granularity+self.ONE_OFF))
        self.is_normalized = False
        # TODO: could be a vector only, since it has the same value for a given label
        # value is the count of patches for each eye label
        self.normalizing_constant = np.zeros((self.number_of_labels, self.predictors_size))
        self.normalize()
        Machine.__init__(self, name=name, address=address)

    def get_indices(self, patch):
        '''

        :param patch: a Patch instance with a response label
        :return: a list of triples corresponding to the 3-dim indices
        '''
        assert(patch.response in range(self.number_of_labels))
        size = self.predictors_size

        # label index
        index0 = np.array([patch.response]*size).astype('int')

        # coordinate index
        index1 = np.array(range(size)).astype('int')

        # value index
        predictors = copy(patch.predictors)

        # # normalize to range [0,1]
        # max_norm = np.max(np.absolute(predictors))
        # if max_norm != 0:
        #     predictors /= max_norm
        # predictors += 1
        # predictors /= 2

        # normalize to range [0,1] using sigmoid function
        factor = 4/self.average_l1
        predictors = np.divide(1, 1+np.exp(-factor*predictors))

        predictors *= self.granularity

        # transforms matrix vector into a list of integers
        index2 = np.floor(predictors).astype('int').T.tolist()[0]

        assert(size == predictors.size)
        return zip(index0, index1, index2)

    def predict_patch(self, patch):
        '''
        Compute the negative log likelihood for each label
        :param patch: an observation of this distribution
        :return: a patch with negative log likelhoods
        '''
        predictors = np.zeros((self.number_of_labels, 1))
        # need to set label in order to compute the likelihood
        for label in range(self.number_of_labels):
            patch.response = label
            indices = self.get_indices(patch)
            negative_log_likelihood = self.negative_log_likelihood(indices=indices)
            predictors[label] = negative_log_likelihood

        label = np.argmin(predictors)
        return Patch(response=label, predictors=predictors)

    def neg_log_prior_patch(self):
        '''
        Computes the patch with the prior probabilities and prior response
        :return: a patch object
        '''
        if not self.is_normalized:
            self.normalize()
        # prior = count/sum(counts)
        priors = self.normalizing_constant[:, 0]
        priors /= np.sum(priors)
        priors = -np.log(priors).reshape((5, 1))
        label = np.argmin(priors)

        return Patch(response=label, predictors=priors)

    def normalize(self):
        self.normalizing_constant = np.sum(self.distribution, axis=2)
        self.is_normalized = True

    def negative_log_likelihood(self, indices):
        # indices = [(label0, 1, 4), (label1, 2, 6), ...]
        if not self.is_normalized:
            self.normalize()
            self.is_normalized = True

        negative_log_likelihood = 0
        for index in indices:
            label = index[0]
            coordinate = index[1]
            normalizer = float(self.normalizing_constant[label, coordinate])
            likelihood = self.distribution[index]/normalizer
            negative_log_likelihood -= log(likelihood)

        return negative_log_likelihood

    def count_patch(self, sparse_patch):
        indices = self.get_indices(sparse_patch)
        for index in indices:
            self.increment_frequency(index)

    def increment_frequency(self, index, count=1):
        self.is_normalized = False
        self.distribution[index] += count

    def show(self):
        # TODO: make it more beautiful - 2d histogram
        if not self.is_normalized:
            self.normalize()
        a = -1
        b = a - a*self.granularity/float(floor(self.granularity/2))
        x = np.linspace(a, b, num=self.granularity+1)

        for label in range(self.number_of_labels):
            matrix = self.distribution[label, :, :]
            normalizer = self.normalizing_constant[label, :]
            normalizer = np.repeat(normalizer, self.granularity+1).reshape((self.predictors_size, self.granularity+1))
            matrix = np.divide(matrix, normalizer)

            assert(round(sum(np.sum(matrix, axis=1))) == self.predictors_size)

            plt.subplot(self.number_of_labels, 1, label)

            for predictor in range(self.predictors_size):
                y = matrix[predictor, :]
                # y = self.distribution[label, predictor, :]
                # y /= self.normalizing_constant[label, predictor]
                plt.plot(x, y)

        number_of_bins = [np.sum(np.argmax(self.distribution, axis=0) == i) for i in range(self.number_of_labels)]
        # [np.sum(self.distribution[:, np.argmax(self.distribution, axis=0) == i])
        # for i  in range(self.number_of_labels)]

        plt.show()

    @staticmethod
    def load(dictionary):
        dict_address = dictionary.get_full_address()
        dist_address = os.path.join(dict_address, "ProbabilityDistribution.pkl")
        if os.path.isfile(dist_address):
            distribution = Abstract.load(dist_address)
            return distribution
        else:
            print "Distribution not found."
            return None


def diabetes_probability_distribution(spark_conf, dictionary):
    number_of_labels = 5
    predictors_size = dictionary.dictionary_size
    granularity = 10
    # ['image,level', '1_right,0','2_left,1', ...]
    files = spark_conf.textFile(TRAIN_DATA).take(10) # TODO: delete take 10
    files = spark_conf.parallelize(files)

    shape = (number_of_labels, predictors_size, granularity)
    shape_size = shape[0]*shape[1]*shape[2]
    accumulators = [spark_conf.accumulator(1) for _ in range(shape_size)]

    # [[1_right,0], [2_left,1], [...], ...]
    eye_label = files.map(lambda s: s.split(",")).filter(lambda l: l[1] in str(range(number_of_labels)))
    # TODO: map it with patch positions
    # [eye_patch1, eye_patch2, ...]
    eye_patches = eye_label.map(lambda l: Eye(Paths.eye_path(l[0]), label=l[1]))\
        .map(lambda eye: eye.next_random_patch(size=dictionary.predictors_size)) # TODO: change next_random_patch

    freq = eye_patches.map(lambda patch: dictionary.predict_patch(patch))\
        .flatMap(lambda patch: probability_distribution.get_indices(patch))\
        .foreach(lambda index: accumulators[np.ravel_multi_index(index, shape)].add(1))

    probability_distribution = ProbabilityDistribution(number_of_labels=5, predictors_size=dictionary.dictionary_size,
                                                       granularity=granularity, address=dictionary.get_full_address())
    accumulators = [accumulator.value for accumulator in accumulators]
    probability_distribution.distribution = np.array(accumulators).reshape(shape=shape)
    return probability_distribution
