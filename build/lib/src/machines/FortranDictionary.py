__author__ = 'marcos'
import numpy as np
from src.abstracts.Observation import Patch
from src.machines.Dictionary import Dictionary
from glmnet.elastic_net import ElasticNet


class FortranDictionary(Dictionary):

    def __init__(self, data_iterator, dictionary_size, penalty, name=None, address=None):
        self.predictors_size = data_iterator.next().predictors.size
        self.dictionary_size = dictionary_size

        if name is None:
            name = "FortranDictionary %dx%d - %f penalty" % \
                   (self.predictors_size, self.dictionary_size, penalty)
        if address is None:
            address = data_iterator.get_full_address()
        Dictionary.__init__(self, data_iterator, dictionary_size, penalty=penalty, name=name, address=address)

        self.lasso_encoder = ElasticNet(alpha=1, rho=penalty)

    def predict(self, observation):
        """
        Computes the sparse coding of the observation
        :param observation:
        :return: A np.matrix vector of shape (dictionary_size, 1)
        """
        self.lasso_encoder.fit(self.dictionary_matrix, observation)
        sparse_code = np.matrix(self.lasso_encoder.coef_)
        assert(sparse_code.shape == (self.dictionary_size, 1))
        return sparse_code

    def predict_patch(self, patch):
        self.lasso_encoder.fit(self.dictionary_matrix, patch.predictors)
        sparse_code = np.matrix(self.lasso_encoder.coef_)
        assert(sparse_code.shape == (self.dictionary_size, 1))
        return Patch(patch.response, predictors=sparse_code)

if __name__=="__main__":
    X = np.random.randn(50, 200)
    w = 3*np.random.randn(200)
    w[10:] = 0
    y = np.dot(X, w)
    enet = ElasticNet(alpha=0.1)
    print enet.coef_
    y_pred = enet.fit(X, y)
    print enet.coef_
    print enet.coef_.shape
    print enet.coef_.__class__
