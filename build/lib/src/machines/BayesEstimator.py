__author__ = 'marcos'
import numpy as np
from src.abstracts.Machine import Machine

# TODO: Divide by L1 Norm of sparse code, since it might be a sufficient statistics for something


class BayesEstimator(Machine):

    def __init__(self, data_iterator, granularity=100, name="BayesEstimator", address="test"):
        self.data_iterator = data_iterator
        self.observation_size = self.data_iterator.next().size
        self.labels = self.data_iterator.labels
        np.ones((self.labels.size, self.observation_size, granularity))
        Machine.__init__(self, name=name, address=address)

    def train(self, training_factor=100):
        pass

    def predict(self, observation):
        pass
