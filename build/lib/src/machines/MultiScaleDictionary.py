__author__ = 'marcos'
import numpy as np
from src.machines import Dictionary
from src.tools import Logger
from time import time
from math import sqrt


class MultiScaleDictionary(Dictionary):

    def __init__(self, data_iterator, patch_dictionary, penalty, name=None, address=None):
        Logger.log_status("Initializing a %s object" % Dictionary.__name__)
        number_of_patches = 4
        self.patch_dictionary = patch_dictionary
        # an iterator with method .next() returning a Patch
        self.data_iterator = data_iterator

        self.predictors_size = self.data_iterator.next().predictors.size
        assert(self.predictors_size == number_of_patches*patch_dictionary.predictors_size)

        self.dictionary_size = 2*number_of_patches*patch_dictionary.dictionary_size

        if name is None:
            name = "MultiScaleDictionary %dx%d - %f penalty" \
                   % (self.predictors_size, self.dictionary_size, self.penalty)
        if address is None:
            address = data_iterator.get_full_address()

        Logger.log_detail("MultiScaleDictionary", "Dictionary dimensions: %s x %s"
                          % (self.predictors_size, self.dictionary_size))

        Dictionary.__init__(self, data_iterator, dictionary_size=self.dictionary_size, penalty=penalty,
                            name=name, address=address)

        # initialize dictionary
        for i in range(number_of_patches*self.patch_dictionary.dictionary_size, self.dictionary_size):
            self.dictionary_matrix[:, i] = self.next_predictor()

        # TODO: correct the indices where dictionary should be located
        for i in range(number_of_patches):
            start_x = i*self.patch_dictionary.predictors_size
            end_x = (i+1)*self.patch_dictionary.predictors_size
            start_y = i*self.patch_dictionary.dictionary_size
            end_y = (i+1)*self.patch_dictionary.dictionary_size
            self.dictionary_matrix[start_x:end_x, start_y:end_y] = self.patch_dictionary

    def train(self, training_time=4000, batch_size=256):
        # TODO: modify for updating only last atoms
        Logger.log_detail("Dictionary", "Dictionary is being trained.")
        assert(self.predictors_size == self.dictionary_matrix.shape[0])
        assert(self.dictionary_size == self.dictionary_matrix.shape[1])

        a_matrix = np.matrix(np.zeros((self.dictionary_size, self.dictionary_size)))
        b_matrix = np.matrix(np.zeros((self.predictors_size, self.dictionary_size)))

        total_load_time = []
        total_code_time = []
        computation_time = time()
        for time_step in range(training_time):
            if time_step % 100 == 0:
                Logger.log_detail("Dictionary", "Training step: %d" % time_step)
            # some constants
            theta = batch_size*time_step if time_step < batch_size else batch_size**2 + time_step - batch_size
            beta = (theta + 1 - batch_size)/(theta + 1)

            a_matrix *= beta
            b_matrix *= beta

            for batch_step in range(batch_size):
                load_time = time()
                predictor = self.next_predictor()
                load_time = time() - load_time
                total_load_time.append(load_time)

                coding_time = time()
                sparse_code = self.predict(predictor)
                coding_time = time() - coding_time
                total_code_time.append(coding_time)

                a_matrix += sparse_code*sparse_code.T
                b_matrix += predictor*sparse_code.T

            for column in range(self.dictionary_size):
                dictionary_atom = self.dictionary_matrix[:, column]
                response = b_matrix[:, column]
                prediction = self.dictionary_matrix*a_matrix[:, column]
                diagonal_element = a_matrix[column, column]

                if diagonal_element != 0:
                    dictionary_atom += (response-prediction)/diagonal_element

                projection_norm = max(1, sqrt(np.power(dictionary_atom, 2).sum()))
                self.dictionary_matrix[:, column] = dictionary_atom/projection_norm

        computation_time = time() - computation_time
        load_ratio = 100*np.sum(total_load_time)/computation_time
        coding_ratio = 100*np.sum(total_code_time)/computation_time
        Logger.log_detail("Dictionary",
                          "Training Time:\nDuration:\t%f\nLoading Ratio:\t%.2f%%\nCoding Ratio:\t%.2f%%"
                          % (computation_time, float(load_ratio), float(coding_ratio)))

        self.compute_statistics()

