__author__ = 'marcos'

import unittest
from src.abstracts.Abstract import Abstract
from src.tools import Logger
import os


class AbstractTest(unittest.TestCase):

    def setUp(self):
        # creates all necessary objects
        # fixture
        self.name = "Abstract"
        self.address = "~/tests/AbstractTest"
        self.object = Abstract(name=self.name, address=self.address)

    def tearDown(self):
        # destroys everything that was created
        unittest.TestCase.tearDown(self)

    def test_attributes(self):
        Logger.log_status(AbstractTest.__name__ + " " + AbstractTest.test_attributes.__name__)
        self.assertEqual(self.object.name, self.name)
        self.assertEqual(self.object.address, self.address)

    def test_save_and_load(self):
        self.file_name = self.object.save()
        self.assertEqual(self.object.name, self.name)
        self.assertEqual(self.object.address, self.address)

        del self.object
        self.assertEqual(os.path.join(self.address, self.name)+".pkl", self.file_name)
        self.object = Abstract.load(file_name=self.file_name)
        self.assertEqual(self.object.name, self.name)
        self.assertEqual(self.object.address, self.address)


if __name__ == '__main__':
    unittest.main()
