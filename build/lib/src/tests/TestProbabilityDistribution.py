__author__ = 'marcos'
import unittest
from skimage import data
from matplotlib import pyplot as plt
import numpy as np
import random
from src.abstracts.Observation import Patch
from src.machines.ProbabilityDistribution import ProbabilityDistribution


class TestProbabilityDistribution(unittest.TestCase):
    SIMPLE_SIZE = 4 # do not change
    SIZE = 30

    SIMPLE_LABELS_SIZE = 2
    LABELS_SIZE = 5

    SIMPLE_GRANULARITY = 2
    GRANULARITY = 100

    def predictors(self):
        variable = np.random.exponential(1, size=self.SIZE)
        return np.matrix(variable).reshape((self.SIZE, 1))

    def simple_predictors(self, label):
        '''
        Vector of bernoullies with probabilities (0.1, 0.5, 0.3, 0.1) for label 0
        and probabilities (0.9, 0.9, 0.9, 0.9) for label 1
        :param label:
        :return:
        '''
        if label == 0:
            variable = np.random.uniform(low=0.0, high=1.0, size=self.SIMPLE_SIZE)
            variable = (variable > np.array([0.1, 0.5, 0.7, 0.9]))*1
            return np.matrix(variable).reshape((self.SIMPLE_SIZE, 1))
        else:
            variable = np.random.uniform(low=0.0, high=1.0, size=self.SIMPLE_SIZE)
            variable = (variable > 0.1)*1
            return np.matrix(variable).reshape((self.SIMPLE_SIZE, 1))

    def setUp(self):
        self.distribution = ProbabilityDistribution(number_of_labels=self.LABELS_SIZE,
                                                    predictors_size=self.predictors().size,
                                                    granularity=self.GRANULARITY)
        self.simple = ProbabilityDistribution(number_of_labels=self.SIMPLE_LABELS_SIZE,
                                                    predictors_size=self.SIMPLE_SIZE,
                                                    granularity=self.SIMPLE_GRANULARITY)

    def tearDown(self):
        pass

    def testConstructor(self):
        self.assertEqual(self.distribution.distribution.shape, (self.LABELS_SIZE, self.SIZE, self.GRANULARITY+1))

    def testGetIndices(self):
        label = 1
        patch = Patch(response=label, predictors=self.predictors())

        indices = self.distribution.get_indices(patch=patch)

        count = 0
        for index0, index1, index2 in indices:
            self.assertEqual(index0, label)
            self.assertEqual(index1, count)
            self.assertGreaterEqual(index2, 0)
            self.assertLessEqual(index2, self.GRANULARITY)
            count += 1

    def testProbabilityEstimation(self):
        for i in range(100000):
            if random.uniform(0, 1) > 0.999:
                label = 0
                patch = Patch(response=label, predictors=self.simple_predictors(label=0))
                self.simple.count_patch(sparse_patch=patch)
            else:
                label = 1
                patch = Patch(response=label, predictors=self.simple_predictors(label=1))
                self.simple.count_patch(sparse_patch=patch)

        print self.simple.distribution
        self.simple.show()

        # negative log likelihood for x_0
        # - n * log P( x_0 == 1 | label = 0) = - n * log 0.9 = 0.105 * n
        # - n * log P( x_0 == 1 | label = 1) = - n * log 0.9 = 0.105 * n
        n = 1000
        coordinate = 0
        success_index = 2

        # label 0
        indices = [(0, coordinate, success_index)] * n
        print self.simple.negative_log_likelihood(indices)

        # label 1
        indices = [(1, coordinate, success_index)] * n
        print self.simple.negative_log_likelihood(indices)

