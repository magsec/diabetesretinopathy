__author__ = 'marcos'
import os
import numpy as np
import pandas

from src.abstracts.Abstract import Abstract
from src.tools.Paths import *
from src.machines.Dictionary import Dictionary


def update_library():
    dictionaries = os.listdir(DIABETES_LIBRARY)
    dictionary_id = range(len(dictionaries))
    library = zip(dictionary_id, dictionaries)

    library_size = len(library)
    statistics = []
    column_labels = ["dict_id", "p", "k", "lambda", "mean_l2_error", "sd_l2_error", "mean_absolute_correlation",
                 "sd_absolute_correlation", "mean_l1", "sd_l1", "mean_l0", "sd_l0"]

    for dict_id, dict_name in library:
        print "Dictionary %d out of %d" % (dict_id, library_size)
        dictionary_path = os.path.join(DIABETES_LIBRARY, dict_name)
        dictionary = Abstract.load(dictionary_path)

        error_l2 = dictionary.error_l2
        absolute_correlation = dictionary.absolute_correlation
        absolute_norm_l1 = dictionary.absolute_norm_l1
        non_zeros_l0 = dictionary.non_zeros_l0

        predictors_size = dictionary.predictors_size
        dictionary_size = dictionary.dictionary_size
        penalty = dictionary.penalty

        statistics.append([dict_id, predictors_size, dictionary_size, penalty,
                           np.mean(error_l2), np.std(error_l2),
                           np.mean(absolute_correlation), np.std(absolute_correlation),
                           np.mean(absolute_norm_l1), np.std(absolute_norm_l1),
                           np.mean(non_zeros_l0), np.std(non_zeros_l0)])

    data = pandas.DataFrame(statistics, columns=column_labels)
    path = os.path.join(DIABETES_RESULTS, "Library.csv")
    data.to_csv(path)


# Main
update_library()








