__author__ = 'marcos'
import os
from src.dataIterators.EyeImageIterator import EyeImageIterator
from src.observations.EyeGreen import EyeGreen
from src.observations.Eye import Eye
from src.tools import Logger
from time import time

train_data = '/home/marcos/Universum/Studium/MasterThesis/data/train/labels.csv'
original_train = '/home/marcos/Universum/Studium/MasterThesis/data/train'
original_test = '/home/marcos/Universum/Studium/MasterThesis/data/test'

green_train = '/home/marcos/Universum/Studium/MasterThesis/data/green_train'
green_test = '/home/marcos/Universum/Studium/MasterThesis/data/green_test'

train_files = os.listdir(green_train)
test_files = os.listdir(green_test)

iterator = EyeImageIterator(train_data, original_train, original_test)
iterator.exclude_from_train(train_files)
iterator.exclude_from_test(test_files)

train_size = len(iterator.train)
test_size = len(iterator.test)


start = time()
counter = 1
print "Train:", counter
eye = iterator.next_train_observation(eye_class=EyeGreen)

while eye is not None:

    try:
        if isinstance(eye, Eye):
            print "\tSaving..."
            eye.save_image(green_train)
    except:
        pass
    try:
        counter += 1
        print "Train:", counter
        eye = iterator.next_train_observation(eye_class=EyeGreen)
    except:
        Logger.log_error("Eye could not be loaded!!!")
        eye = object()

    if counter > train_size:
        eye = None
end = time()
print end-start


start = time()
counter = 1
print "Test:", counter
eye = iterator.next_test_observation(eye_class=EyeGreen)

while eye is not None:

    try:
        if isinstance(eye, Eye):
            print "\tSaving..."
            eye.save_image(green_test)
    except:
        pass
    try:
        counter += 1
        print "Test:", counter
        eye = iterator.next_test_observation(eye_class=EyeGreen)
    except:
        Logger.log_error("Eye could not be loaded!!!")
        eye = object()

    if counter > test_size:
        eye = None

end = time()
print end-start
