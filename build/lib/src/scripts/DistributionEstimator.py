__author__ = 'marcos'
import numpy as np
from src.dataIterators.SparsePatchIterator import SparsePatchIterator
from src.machines.ProbabilityDistribution import ProbabilityDistribution
from src.explorers.DictionaryExplorer import DictionaryExplorer
from src.tools.Paths import DIABETES_LIBRARY, TRAIN_DATA, GREEN_TRAIN, GREEN_TEST


def main():
    train_size = 100000

    dictionary = DictionaryExplorer(DIABETES_LIBRARY).choose_dictionary()
    sparse_patch_iterator = SparsePatchIterator(dictionary, data_file=TRAIN_DATA, train_path=GREEN_TRAIN,
                                                test_path=GREEN_TEST, train_size=train_size)
    distribution = ProbabilityDistribution(number_of_labels=5, predictors_size=sparse_patch_iterator.predictors_size,
                                           granularity=10, address=dictionary.get_full_address(), average_l1=2)

    counter = 0
    sparse_patch = sparse_patch_iterator.next_train_observation()

    while sparse_patch is not None:
        distribution.count_patch(sparse_patch)
        sparse_patch = sparse_patch_iterator.next_train_observation()
        counter += 1

        if counter % 1000000 == 0:
            distribution.save()

    distribution.save()
    print np.max(distribution.distribution)
    print np.sum(distribution.distribution, axis=1)















if __name__=="__main__":
    main()