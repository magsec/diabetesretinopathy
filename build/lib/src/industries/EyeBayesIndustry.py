__author__ = 'marcos'
import os
import numpy as np
from pandas import DataFrame
from src.abstracts.Industry import Industry
from src.abstracts.Observation import Patch
from src.machines.ProbabilityDistribution import ProbabilityDistribution


class EyeBayesIndustry(Industry):

    def __init__(self, eye_iterator, dictionary, name="EyeBayesIndustry", address=None):
        '''

        :param eye_iterator: an iterator over the data
        :param machines: a list of machines
        :param name:
        :param address:
        :return:
        '''
        self.eye_iterator = eye_iterator

        self.dictionary = dictionary
        self.patch_size = dictionary.predictors_size

        self.distribution = ProbabilityDistribution.load(self.dictionary)
        if self.distribution is None:
            raise ValueError("Distribution could not be loaded")
        if address is None:
            address = self.dictionary.get_full_address()

        self.number_of_labels = self.distribution.number_of_labels

        # dictionary: key=eye_id, value=Patch object
        self.product = {}

        Industry.__init__(self, name, address)

    def produce_test(self):
        '''
        Computes the posterior probability of the labels for each eye
        :return:
        '''
        eye_limit = self.eye_iterator.total_test
        eye_counter = 0

        eye = self.eye_iterator.next_test_observation()
        while eye is not None:
            product_id = eye.name

            patch = eye.next_patch(self.patch_size)
            patch_limit = eye.number_of_patches
            patch_counter = 0

            neg_log_likelihood_patch = self.distribution.neg_log_prior_patch()

            while patch is not None:
                sparse_patch = self.dictionary.predict_patch(patch=patch)
                negll_patch = self.distribution.predict_patch(sparse_patch)

                neg_log_likelihood_patch.predictors += negll_patch.predictors

                patch = eye.next_patch(self.patch_size)
                patch_counter += 1
                if patch_counter >= patch_limit:
                    patch = None

            neg_log_likelihood_patch.response = np.argmin(neg_log_likelihood_patch.predictors)
            self.product[product_id] = neg_log_likelihood_patch
            print "\tPredicted label: %d" % neg_log_likelihood_patch.response
            print "\tPredicted likelihoods:\n", neg_log_likelihood_patch.predictors.T
            eye = self.eye_iterator.next_test_observation()
            eye_counter += 1
            if eye_counter >= eye_limit:
                eye = None
                self.save()
            if eye_counter % 1000 == 0:
                self.save()

    def export_production(self):
        result = [[k, self.product[k].response] for k in self.product]
        df = DataFrame(data=result, columns=['image', 'level'])
        out_path = os.path.join(self.get_full_address(), 'submission.csv')
        if not os.path.exists(path=self.get_full_address()):
            os.makedirs(self.get_full_address())
        df.to_csv(out_path, index=False)
