__author__ = 'marcos'
from pandas import read_csv
import numpy as np


class TripFormatter(object):

    def __init__(self):
        pass

    @staticmethod
    def get_trip(trip_path):
        df = read_csv(trip_path)
        list = [i for pair in zip(df.iloc[:, 0], df.iloc[:, 1]) for i in pair]
        predictors = np.matrix(np.array(list)).T

    @staticmethod
    def format_trip_patch(trip_patch):
        pass
