__author__ = 'marcos'
import numpy as np
from numpy.linalg import norm


def normalize(array):
    vector = array.flatten()
    vector = vector - np.mean(vector)
    size = norm(vector, 2)
    # # TODO: DELETE FOLLOWING LINE
    # return vector
    if size == 0:
        return vector
    else:
        return vector/size


def bernoulli_mutual_info(bernoulli_data):
    pass